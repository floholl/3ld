function xyz = sph3cart(azimuth,elevation,radius)

%3LD :: XYZ = SPH3CART(AZIMUTH,ELEVATION,RADIUS) Conversion from spherical to matrix with cartesian coordinates.
%   This function is identical to Matlab's SPH2CART, but returns a single
%   N-by-3 matrix as an input argument, rather than three separate arrays.
%   The columns of XYZ represent the x, y, and z components of the N
%   different points. The azimuth, elavtion and radius components have to
%   be provided as three independent input arguments. This function is
%   convenient for converting the spherical coordinates of a vertices array
%   back to their cartesian representation after edits like radius mapping.
%
%   See also: CART3SPH (3LD).


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ------------------------ INPUT ARGUMENT CHECKING ------------------------

% Check number of input arguments
if nargin < 3
    error('Not enough input arguments.')
elseif nargin > 3
    error('Too many input arguments.')    
end

% Check input arguments
if ~isnumeric(azimuth) || ~isnumeric(elevation) || ~isnumeric(radius)
    error('Invalid input arguments. Must be numerical arrays.');
end

% Convert to column vectors
azimuth = azimuth(:);
elevation = elevation(:);
radius = radius(:);


% ----------------------------- CALCULATIONS -----------------------------

% Use SPH2CART for conversion, since it can also handle arrays of different
% size, like in SPH2CART(1:3,1:3,2)
[x,y,z] = sph2cart(azimuth,elevation,radius);

% Build output array
xyz = [x,y,z];

return