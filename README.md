# 3LD - Library for Loudspeaker Layout Design

(c) 2006-2020 by Florian Hollerweger

3LD has been implemented as a student's project at the Institute of Electronic Music and Acoustics at the University of
Music and Dramatic Arts Graz, Austria. It is a Matlab library containing files for the generation and evaluation of
three-dimensional loudspeaker layouts and includes functions for building Platonic solids, a bucky ball, geodesic spheres,
and minimal energy configurations, as well as implementations of different sound spatialization algorithms (Vector Base
Panning and Higher Order Ambisonics). Also, functions for soundfield rendering and evaluation of reconstructed soundfields
are available.

In the Matlab command window, type `help <functionname>` to receive a detailed description of any 3LD function.
A comprehensive [30-page reference manual](https://gitlab.com/floholl/3ld/-/blob/master/3LD.pdf) is also included with the distribution.

3LD is published under the GNU General Public License. A copy of the license is included in the "COPYING" file which
is part of the distribution. For more information you can also refer to the copyright notes in the .m files.

Contact:
Florian Hollerweger
flo@mur.at