function B = amb3d_encoder(order,sourceDirection,sourceGain,areSourcesIdentical,rowOrder)

%3LD :: AMB3D_ENCODER Ambisonic encoded channel gains.
%   B = AMB3D_ENCODER(M,SRC[,GAIN,IDENTICAL,SORT]) calculates the
%   weightings of the spherical harmonic components of a 3D Ambisonic
%   soundfield. The N3D encoding convention as specified in [1] is applied.
%
%   M is the Ambisonic order and determines the number of Ambisonic
%   channels N = (M+1)^2, which is the number of rows in the field 'gain'
%   of the output structure B.
%
%   SRC specifies the directions of the virtual sound sources. It is an
%   S-by-2 array representing azimuth and elevation in radians. The same
%   left-oriented coordinate system as in Matlab's SPH2CART is applied.
%
%   GAIN specifies the gains of the virtual sound sources. It can be a
%   scalar, in which case it is applied to all sources, or an S-by-1 array,
%   with different gain factors for each source. Additional values will be
%   ignored, and missing values will be set to 1. If not specified, GAIN
%   defaults to 1 for all sources.
%
%   IDENTICAL specifies whether the spatialized sound sources are fed by the
%   same audio signal. If IDENTICAL is not zero, this is assumed to be the
%   case, while independent sources are assumed if IDENTICAL is 0. This
%   affects the dimensions of the output array B.gain. If not specified,
%   IDENTICAL defaults to 0.
%
%   SORT is a string which affects the order of the rows in the output
%   array. For 'spharmonic' (or 'sph' or 's'), the order of rows matches
%   the one of 3LD's SPHARMONIC, while for 'ambisonic' (or 'amb' or 'a'),
%   the Ambisonic channels are sorted according to a convention used in
%   [1]. If not specified, SORT defaults to 'spharmonic'.
%
%   The output structure B has two fields. B.gain is an N-by-S matrix if
%   IDENTICAL=0 and an N-by-1 matrix if IDENTICAL=1 and represents the
%   weightings of the N spherical harmonic components. In the latter case,
%   all sound sources are fed by the same signal and their weightings are
%   superponed. B.sort is equivalent to the SORT input argument. It is
%   added to the output structure for later decoding with 3LD's
%   AMB3D_DECODER.
%
%   Example:
%   Encode a front and a top source at third order
%       source_position = [0 0;0 pi/2];
%       source_gain = [1 0.5];
%       B = amb3d_encoder(3,source_position,source_gain)
%
%   See also: AMB3D_DECODER (3LD), AMB3D_REGULARITY (3LD), VBP (3LD),
%             SPHARMONIC (3LD).
%   
%   Reference:
%     [1] J. Daniel, "Représentation de champs acoustiques, applications a
%         la transmission et a la reproduction de scènes sonores complexes
%         dans un contexte multimédia", PhD thesis, University of Paris 6,
%         2000


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ----- INPUT ARGUMENT CHECKING AND PROCESSING, CONSTANT DEFINITIONS -----

% Check number of input arguments
if nargin < 2
    error('Not enough input arguments.')
elseif nargin > 5
    error('Too many input arguments.')    
end


% Check 'order' input argument
if ~isnumeric(order) || ~isscalar(order) || order < 0
    error('''Unknown ''order'' input argument. Must be non-negative integer.');
end
% Number of Ambisonic channels for the given order: N = (M+1)^2
numChannels = (order + 1)^2;


% Check 'sourceDirection' input argument
if size(sourceDirection,2) ~= 2
    error('Illegal dimensions of ''sourceDirection'' input array. Must be numSources-by-2 array.');
end
sourceAz = sourceDirection(:,1);
sourceElev = sourceDirection(:,2);
% Number of virtual sound sources
numSources = size(sourceDirection,1);


% Check 'sourceGain' input argument
if  ~exist('sourceGain','var') || isempty(sourceGain)
    % Default to 1 if not specified
    sourceGain = 1;
end
if isscalar(sourceGain)
    % Apply to all sources if specified as a scalar
    sourceGain = repmat(sourceGain,1,numSources);
else
    if numel(sourceGain) > size(sourceDirection,1)
        % Ignore additional values
        sourceGain = sourceGain(1:size(sourceDirection,1));
        warning('Clipped oversized ''sourceGain'' input array to required amount of entries.')
    elseif numel(sourceGain) < size(sourceDirection,1)
        % Set missing values to one
        sourceGain(end+1:size(sourceDirection,1)) = 1;
        warning('Set missing entries in ''sourceGain'' input array to one.');
    end
    % Make this a row vector for internal calculations
    sourceGain = sourceGain(:)';
end


% Check 'areSourcesIdentical' input argument
if ~exist('areSourcesIdentical','var') || isempty(areSourcesIdentical)
    % Default to 0 if not specified
    areSourcesIdentical = 0;
end


% Check 'rowOrder' input argument
if ~exist('rowOrder','var') || isempty(rowOrder)
    rowOrder = 'spharmonic';
else
    % Safety
    rowOrder = lower(rowOrder);
    if strcmp(rowOrder,'spharmonic') || strcmp(rowOrder,'sph') || strcmp(rowOrder,'s')
        rowOrder = 'spharmonic';
    elseif strcmp(rowOrder,'ambisonic') || strcmp(rowOrder,'amb') || strcmp(rowOrder,'a')
        rowOrder = 'ambisonic';
    else
        error('Unknown ''rowOrder'' input argument. Must be ''spharmonic''/''s'' or ''ambisonic''/''a''.');
    end
end
% Assign row order in according field of output array
B.sort = rowOrder;



% ----------------------------- CALCULATIONS -----------------------------

% First do zeroth-order W channel (= "mono" channel)
B.gain = spharmonic(0,sourceAz,sourceElev,'sch');


% Then do orders > 0
% TODO: FOR LOOP MIGHT BE AVOIDABLE IN FUTURE VERSIONS BY INTRODUCING A
% SMARTER INDEX MAP FOR CHANNEL RE-ORDERING (WHEN rowOrder = 'ambisonic')
for thisOrder = 1 : order
    
    newSpharmonics = spharmonic(thisOrder,sourceAz,sourceElev,'sch');
    
    if strcmp(rowOrder,'ambisonic')
        
        % Rearrange order of spherical harmonics functions, e.g. for order = 3
        % from m = -3,-2,-1,0,1,2,3 to m = 3,-3,2,-2,1,-1,0, following the
        % Ambisonic conventions
        
        % Initialize index map with linear index
        idxMap = 1 : 2*thisOrder+1;
        
        down = 2*thisOrder+1:-1:thisOrder+1;
        up = 1:thisOrder;
        
        % Interleave vectors
        for i = 1 : thisOrder
            j = 2*i;
            idxMap(j-1) = down(i);
            idxMap(j)   = up(i);
        end
        % Add last forgotten element
        idxMap(2*thisOrder+1) = thisOrder + 1;
        
        % Make column vector
        idxMap = idxMap';
        
        % Rearrange spherical harmonic functions according to idxMap
        newSpharmonics = newSpharmonics(idxMap,:);

    end
    
    % Add new spherical harmonics to output array
    B.gain = [B.gain ; newSpharmonics];
    
end


% Now we are going to convert the Ambisonic channels from SN3D to the
% N3D encoding convention:

% Build vector 'orderOfChannels', containing the order of each
% Ambisonic channel
orderOfChannels = 0;    % Initialize with W channel element
% TODO: FOR-LOOP COULD BE AVOIDED IN A FUTURE VERSION
for m = 1:order
    orderOfChannels(end+1 : end+2*m+1) = m;
end
% Convert to row vector for internal calculations
orderOfChannels = orderOfChannels';

% Calculate conversion factors according to p.156 in [1]
conversionFactors = sqrt(2*orderOfChannels+1);

% Convert from SN3D to N3D encoding convention
conversionFactors = repmat(conversionFactors,1,numSources);
B.gain = B.gain .* conversionFactors;


% Apply gains of the individual sound sources to the encoded output
sourceGain = repmat(sourceGain,numChannels,1);
B.gain = B.gain .* sourceGain;


% Superpone the ambisonic channel gains in the case of identical source signals
if areSourcesIdentical ~= 0
    B.gain = sum(B.gain,2);
end

return