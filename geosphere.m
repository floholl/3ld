function polyhedron = geosphere(shape,freq,radius)

%3LD :: GEOSPHERE Geodesic spheres.
%   P = GEOSPHERE(SHAPE[,FREQ,RADIUS]) generates geodesic spheres from one
%   of the five platonic solids using 3LD's PLATONICSOLID, or from an
%   arbitrary vertices/faces structure at its input. Geodesic spheres are
%   constructed either by adding a vertex in the middle of each facet in a
%   polyhedron and connecting it to every other vertex in the facet
%   (fig.(a)), or by subdividing the edges of each facet and connecting the
%   new vertices in a way that depends on the facet's shape (figs.(b),(c)).
%   The first approach can be applied to arbitrarily shaped facets, while
%   the latter can only be applied to triangles or rectangles. Here, the
%   'frequency' f of the geodesic sphere determines into how many parts
%   each edge is subdivided (fig.(b): f=2, fig.(c): f=3) Eventually, the
%   new vertices are pushed out to the radius of the sphere circumscribing
%   the polyhedron. The process can be repeated iteratively.
%
%    (a)                (b)           (c)   C
%  D ____ C         D _______ C            /\
%   |\  /|          |   |   |             /__\
%   | \/ |          |___|___|            /\  /\
%   | /\ |          |   |   |           /__\/__\
%   |/__\|          |___|___|          /\  /\  /\
%  A      B         A        B      A /__\/__\/__\ B
%
%
%   SHAPE can be a string specifying a platonic solid from which to build a
%   geodesic sphere, or an existing polyhedron. In the first case, choices
%   are 'tetrahedron'/'tetra', 'hexahedron'/hex'/'cube', 'octahedron'/'oct',
%   'dodecahedron'/'dodec', 'icosahedron'/'ico'. In the latter case, SHAPE
%   has to be a structure with fields 'vertices' and 'faces'. 'vertices' is
%   a V-by-3 matrix with rows representing the x,y,z coordinates of the V
%   vertices (same coordinate system as in Matlab's CART2SPH applied), and
%   'faces' is a F-by-S matrix with each row listing the row indices of the
%   vertices forming one of the F faces of the polyhedron. S refers to the
%   number of vertices in a face of the polyhedron.
%
%   FREQ is a matrix which determines the frequencies applied in the
%   tessellation and the number of iterations. The j-th column refers to
%   the j-th iteration. The i-th row refers to the polygons with j minus 2
%   vertices, i.e. triangles for the first row, rectangles for the second,
%   pentagons for the third, and so on. For example, the element (4,3) of
%   FREQ specifies the frequency applied to any hexagon in our polyhedron
%   in the third iteration. Possible values for FREQ are:
%       FREQ = 0 -> faces are not tessellated
%       FREQ = 1 -> faces are midpoint-triangulated
%       FREQ > 1 -> triangular faces are triangulated, and rectangular
%                   faces are rectangulated at the frequency FREQ.
%   Note that since any faces with more than 4 vertices can only be
%   midpoint-triangulated, elements >1 in the rows i>3 will be clipped to
%   1. If FREQ has less rows than there are different facet shapes in the
%   polyhedron, the missing rows will be filled up with the entries of the
%   last available row. For example, FREQ=[0; 3] means that all triangles 
%   will not be modified, all rectangles will be rectangulated at frequency
%   3, and all faces with more than 4 vertices will be midpoint-
%   triangulated, since that's our only option. If not specified, FREQ
%   defaults to 2, i.e. triangles are triangulated at frequency 2,
%   rectangles rectangulated at frequency 2, and pentagons, etc. are
%   midpoint-triangulated.
%
%   RADIUS refers to the radius of the geodesic sphere, i.e. the distance
%   of its vertices to the center of the sphere. It can be either a scalar
%   or a handle to a function with two arguments, the first of which is
%   interpreted as azimuth, and the second one as elevation. In the first
%   case, all vertices are set to the same radius specified by the scalar.
%   In the second case, each vertex is set to the radius specified by the
%   radius function for its respective direction. Note that in both cases,
%   the radii of existing vertices will be overwritten if SHAPE is an
%   existing polyhedron. If RADIUS is empty or not specified, the
%   polyhedron will be tessellated, but the new vertices will not be pushed
%   out anywhere.
%
%   P is a structure with fields 'vertices', and 'faces', which provide the
%   same properties as required for the SHAPE input argument. It can be
%   poltted directly using 3LD's PLOT3LD or Matlab's PATCH.
%
%   Examples:
%       (1) p = geosphere('oct',2,1); plot3LD(p);
%       (2) b = bucky2; p = geosphere(b,1,1); plot3LD(p);
%       (3) p = geosphere('oct',[2 3]); plot3LD(p);
%       (4) radius = @(az,elev) abs(cos(az) .* cos(elev)) + 2;
%       	p = geosphere('oct',[2 2 2],radius); plot3LD(p);
%
%   See also: PLATONICSOLID (3LD), MINENERGYCONF (3LD), SPHERE, ELLIPSOID,
%             CYLINDER, PATCH.


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger (floholl_AT_sbox.tugraz.at) and
% (c) 2002 Darren Weber.
% Parts of this function base on Matlab code originally written by Darren
% Weber, which is published under the GPL and available at
% http://www.mathworks.com/matlabcentral/fileexchange/loadFile.do?objectId=1877&objectType=file

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ----- INPUT ARGUMENT CHECKING AND PROCESSING, CONSTANT DEFINITIONS -----

% Check number of input arguments
if nargin < 1
    error('Not enough input arguments.')
elseif nargin > 3
    error('Too many input arguments.')    
end


% Check 'shape' input argument
if ischar(shape)
    % If 'shape' is a string, build an according platonic solid
    polyhedron = platonicsolid(shape);
elseif isstruct(shape) && isfield(shape,'vertices') && isfield(shape,'faces')
    % If 'shape' is a vertices/faces structure, interpret as polyhedron
    polyhedron = shape;
end

% Safety: remove all columns from facet matrix that only contain NaNs
polyhedron.faces(:,all(isnan(polyhedron.faces))) = [];

% Check for illegal facets with less than three vertices
if polyhedron.faces < 3
    error('Faces field of polyhedron contains illegal faces with less than three non-NaN vertices');
end


% Check 'radius' input argument
pushToRadius = 1;
if  ~exist('radius','var') || isempty(radius)
    % Only tessellate, but do not push out new vertices
    pushToRadius = 0;
elseif isa(radius,'function_handle')
    % Set radius of vertices according to specified radius function
    radiusFct = radius;
elseif isnumeric(radius) && isscalar(radius)
    radiusFct = @(az,elev) radius;
else
    error('Unknown ''radius'' input argument. Must be function handle or numeric scalar.');
end


% Check 'freq' input argument
if ~exist('freq','var') || isempty(freq)
    freq = 2;
    % Later, this will translate to: triangulate triangles at
    % frequency 2, rectangulate rectangles at frequency 2,
    % and midpoint-triangulate all other faces in a single iteration
elseif ~isnumeric(freq) | any(freq(:)<0) | floor(freq) ~= freq
    error('Unknown ''freq'' input argument. Must be array of non-negative integers.');
end

% Number of iterations is determined by number of columns in 'freq' array
numIterations = size(freq,2);

maxNumVert = size(polyhedron.faces,2);
if maxNumVert - 2 < size(freq,1)
    % If frequencies are specified for more different facet shapes than
    % available, ignore the higher rows of the freq matrix
    freq = freq(1:maxNumVert-2,:);
elseif maxNumVert - 2 > size(freq,1)
    % If frequencies are specified for less different facet shapes than we
    % need, copy the last row of the freq matrix to the missing rows.
    
    freq = [freq; repmat(freq(size(freq,1),:),maxNumVert-2-size(freq,1),1)];
end

% All faces with more than 4 vertices (pentagons, hexagons, etc.) can only
% be midpoint-triangulated, so set their frequencies to 1.
if size(freq,1) >= 2
    freq = [freq(1:2,:) ; freq(3:end,:) ~= 0];
end


% ----------------------------- CALCULATIONS -----------------------------

for i = 1:numIterations
    
    % Get the number of faces in the polyhedron
    numFaces = size(polyhedron.faces,1);
    
    % Initialization for later use of 'end'
    newFaces = [];
       
    for facIdx = 1:numFaces
        
        % The facet shape (number of vertices in a facet) is determined by
        % the number of non-NaN vertices in a facet
        verticesInFacet = sum(~isnan(polyhedron.faces(facIdx,:)));
        
        % Determine frequency for this facet shape and iteration
        thisFreq = freq(verticesInFacet-2,i);
        
        switch thisFreq
            
            case 0
                % Ignore faces of this shape in this iteration
                newFaces(end+1,:) = polyhedron.faces(facIdx,:);
                
            case 1
                % Midpoint-triangulate the facet, whatever its shape is
                [polyhedron,newFaces] = triangulatePolygon(polyhedron,newFaces,facIdx,verticesInFacet);
                
            otherwise
                if verticesInFacet == 3
                    % Triangulate triangle at specified frequency
                    [polyhedron,newFaces] = triangulateTriangle(polyhedron,newFaces,facIdx,thisFreq);
                elseif verticesInFacet == 4
                    % Rectangulate rectangle at specified frequency
                    [polyhedron,newFaces] = rectangulateRectangle(polyhedron,newFaces,facIdx,thisFreq);
                end
                
        end
          
    end
    
    % Replace original faces
    polyhedron.faces = newFaces;
    
   % Remove any columns we might have created that contain only NaNs (e.g.
   % when midpoint-triangulating pentagons)
    polyhedron.faces(:,all(isnan(polyhedron.faces))) = [];
    
end


if pushToRadius
    % Push all vertices to the specified radius
    [az,elev] = cart2sph(polyhedron.vertices(:,1),polyhedron.vertices(:,2),polyhedron.vertices(:,3));
    r = radiusFct(az,elev);
    r = r(:);   % Required safety
    [polyhedron.vertices(:,1),polyhedron.vertices(:,2),polyhedron.vertices(:,3)] = sph2cart(az,elev,r);
end
    

return



% ------------------------------SUBFUNCTIONS------------------------------


    function [polyhedron,newFaces] = triangulatePolygon(polyhedron,newFaces,facIdx,verticesInFacet)

    % Get a column vector of the indices of the vertices in this facet
    idx = polyhedron.faces(facIdx,:)';
    % Remove NaNs from it
    idx = idx(~isnan(idx));
    
    % Get a N-by-3 matrix of their coordinates too
    vertices = polyhedron.vertices(idx,:);

    % Get the midpoint of the facet
    midPoint = sum(vertices,1)/verticesInFacet;
    
    % Add the new midpoint at the end of the vertex list
    polyhedron.vertices(end+1,:) = midPoint;
    
    % Now, the index of the midpoint equals the length of the vertex list
    idxMid = size(polyhedron.vertices,1);
    
    % Create new facets, e.g. [A B M ; B C M ; C D M ; D E M ; E A M] for a pentagon
    for v = 1 : verticesInFacet
        newFacet = [idx(v) idx(mod(v,verticesInFacet)+1) idxMid];
        newFacet(:,end+1:size(polyhedron.faces,2)) = NaN;   % Fill up with NaNs
        newFaces(end+1,:) = newFacet;
    end

    return
        
        
    
    function [polyhedron,newFaces] = triangulateTriangle(polyhedron,newFaces,facIdx,freq)

    % Initialize newVertices for later use of 'end'
    newVertices = [];

    % Get the indices of the vertices of this facet
    idxA = polyhedron.faces(facIdx,1);
    idxB = polyhedron.faces(facIdx,2);
    idxC = polyhedron.faces(facIdx,3);
    
    % Remove NaNs from them
    idxA = idxA(~isnan(idxA));
    idxB = idxB(~isnan(idxB));
    idxC = idxC(~isnan(idxC));

    % Get their coordinates too
    A = polyhedron.vertices(idxA,:);
    B = polyhedron.vertices(idxB,:);
    C = polyhedron.vertices(idxC,:);

    % Create vertices on the edges AC, BC, including A,B,C themselves
    delta = (0:freq)';
    AC = repmat(A,length(delta),1) + kron(C-A,delta) / freq;
    BC = repmat(B,length(delta),1) + kron(C-B,delta) / freq;
    
    % Initialize for later use of x = [x y]
    vertices = [];
    tempFreq = freq;
    
    % Create new vertices _within_ the facet
    % TODO: REPLACE FOR LOOP BY 3D MATRIX+RESHAPE IN FUTURE VERSION
    for i = 1:size(AC,1)-1  % Only up to size(AC,1)-1, since last entry is C itself (avoid division by zero in the for loop)
        newVertices = repmat(AC(i,:),tempFreq+1,1) + kron(BC(i,:)-AC(i,:),(0:tempFreq)') / tempFreq;
        vertices = [vertices; newVertices];
        tempFreq = tempFreq-1;
    end
    vertices = [vertices;C];    % Before we ignored C, so add it now
    
    % Check for existing vertices
    [polyhedron,vertIdx] = checkVertices(polyhedron,vertices);
    
    % This stuff we need for creating our new facets
    newVerticesPerFacet = ( (freq+1)^2 + freq+1 ) / 2;  % Remember Gauss
    fromZeroToFreq = 0:freq;
    gaussFromZeroToFreq = (fromZeroToFreq.^2 + fromZeroToFreq)./2;
    BCedgeVertices = newVerticesPerFacet - gaussFromZeroToFreq;
    ACedgeVertices = newVerticesPerFacet - (gaussFromZeroToFreq+(0:freq));
    freqEdited = [];    % Initialize for later use of 'end'
    for i = 1:length(fromZeroToFreq)
        freqEdited(end+1:end+fromZeroToFreq(i)+1) = i-1;
    end
    freqEdited = rot90(freqEdited);

    % Create new facets
    for i = 1 : newVerticesPerFacet-2   % We can stop 2 vertices before the last one
        % Leave out vertices at the edges of the facet
        if ~ismember(i,BCedgeVertices)
            newFacet = [vertIdx(i), vertIdx(i+1), vertIdx(i+freqEdited(i)+1)];
            newFacet(:,end+1:size(polyhedron.faces,2)) = NaN;   % Fill up with NaNs
            newFaces(end+1,:) = newFacet;
        end
        if ~ismember(i,ACedgeVertices) & ~ismember(i,BCedgeVertices)
            newFacet = [vertIdx(i), vertIdx(i+freqEdited(i)+1), vertIdx(i+freqEdited(i))];
            newFacet(:,end+1:size(polyhedron.faces,2)) = NaN;   % Fill up with NaNs
            newFaces(end+1,:) = newFacet;
        end
    end

    return


    
    function [polyhedron,newFaces] = rectangulateRectangle(polyhedron,newFaces,facIdx,freq)

    % Get the indices of the vertices of this facet
    idxA = polyhedron.faces(facIdx,1);
    idxB = polyhedron.faces(facIdx,2);
    idxC = polyhedron.faces(facIdx,3);
    idxD = polyhedron.faces(facIdx,4);
    
    % Remove NaNs from them
    idxA = idxA(~isnan(idxA));
    idxB = idxB(~isnan(idxB));
    idxC = idxC(~isnan(idxC));
    idxD = idxD(~isnan(idxD));

    % Get their coordinates too
    A = polyhedron.vertices(idxA,:);
    B = polyhedron.vertices(idxB,:);
    C = polyhedron.vertices(idxC,:);
    D = polyhedron.vertices(idxD,:);
    
    % Create vertices on the edges AB, DC, including A,B,C,D themselves
    delta = (0:freq)';
    AB = repmat(A,length(delta),1) + kron(B-A,delta) / freq;
    DC = repmat(D,length(delta),1) + kron(C-D,delta) / freq;
    
    % Initialize for later use of x = [x y]
    vertices = [];
    
    % Create new vertices _within_ the facet
    % TODO: REPLACE FOR LOOP BY 3D MATRIX+RESHAPE IN FUTURE VERSION
    for i = 1:size(AB,1)
        newVertices = repmat(AB(i,:),length(delta),1) + kron(DC(i,:)-AB(i,:),delta) / freq;
        vertices = [vertices; newVertices];
    end
    
    % Check for existing vertices
    [polyhedron,vertIdx] = checkVertices(polyhedron,vertices);
    
    % Create new facets
    for i = 1:(freq*(freq+1)-1)   % We can stop 1 vertex before the last one
        % Leave out vertices at the edges of the facet
        if mod(i,freq+1) ~=0
            newFacet = [vertIdx(i), vertIdx(i+1+freq), vertIdx(i+1+freq+1), vertIdx(i+1)];
            newFacet(:,end+1:size(polyhedron.faces,2)) = NaN;   % Fill up with NaNs
            newFaces(end+1,:) = newFacet;
        end
    end

    return
    
    
    
    function [polyhedron,vertIdx] = checkVertices(polyhedron,vertices)

    % For each of the new vertices...
    for i = 1:size(vertices,1)
        
        % Check for existing vertices at the position of this vertex
        vertRep = repmat(vertices(i,:),size(polyhedron.vertices,1),1);
        % Account for the possibility of numerical inaccuracies by
        % interpreting very similar coordinates as identical
        Vexist = find ( abs(polyhedron.vertices(:,1) - vertRep(:,1)) < 0.0001 & ...
                        abs(polyhedron.vertices(:,2) - vertRep(:,2)) < 0.0001 & ...
                        abs(polyhedron.vertices(:,3) - vertRep(:,3)) < 0.0001 );
         
        if Vexist
            % If a vertex already exists there, use that one
            vertIdx(i) = Vexist;
        else
            % Otherwise, add the new vertex at the end of the vertex list
            polyhedron.vertices(end+1,:) = vertices(i,:);
            % Now, the index of the vertex equals the length of the list
            vertIdx(i) = size(polyhedron.vertices,1);
        end
        
    end
    
    return