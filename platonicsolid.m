function polyhedron = platonicsolid(shape,radius)

%3LD :: PLATONICSOLID Vertices/faces structure of platonic solids.
%   P = PLATONICSOLID(SHAPE[,RADIUS]) generates one of the
%   five convex regular polyhedra, which are also refered to as 'Platonic
%   solids'. Those are the tetrahedron, the hexahedron (cube), the
%   octahedron, the dodecahedron, and the icosahedron.
%
%   SHAPE is a string specifying which polyhedron to generate: Choices are
%   'tetrahedron'/'tetra', 'hexahedron'/hex'/'cube', 'octahedron'/'oct',
%   'dodecahedron'/'dodec', 'icosahedron'/'ico'.
%
%   RADIUS refers to the radius of the Platonic solid, i.e. the distance of
%   its vertices to the center of the polyhedron, which is always located
%   at the origin of the coordinate system. If not specified, RADIUS
%   defaults to 1.
%
%   P has fields 'vertices' and 'faces.' 'vertices' is a V-by-3 matrix with
%   rows representing the x,y,z coordinates of the V vertices (the same
%   cartesian coordinate system as in Matlab's SPH2CART is applied), and
%   'faces' is a F-by-S matrix with each row listing the row indices of the
%   vertices forming one of the F faces of the polyhedron. S refers to the
%   number of vertices in a face of the polyhedron: The tetrahedron,
%   octahedron, and icosahedron have triangular faces (S=3), the cube has
%   rectangular faces (S=4), and the dodecahedron has pentagonal faces
%   (S=5). P can be plotted directly using Matlab's PATCH or 3LD's PLOT3LD.
%
%   Example:
%   Plot an icosahedron:
%       p = platonicsolid('ico');
%       plot3LD(p);
%
%   See also: GEOSPHERE (3LD), MINENERGYCONF (3LD), SPHERE, ELLIPSOID,
%             CYLINDER, PATCH.


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at
% Parts of this function base on code originally written by Jon Leech
% (leech @ cs.unc.edu); icosahedral code added by Jim Buddenhagen
% (jb1556@daditz.sbc.com) 5/93; adapted from C to Matlab by
% Darren.Weber_at_radiology.ucsf.edu 06/2002; reorder of the faces for the
% 'ico' surface so they are indeed clockwise!  Now the surface normals are
% directed outward, 5/2004; published under the GNU GPL by Darren Weber and
% available at
% http://www.mathworks.com/matlabcentral/fileexchange/loadFile.do?objectId=1877&objectType=file
% Hexahedron and dodecahedron code added by Florian Hollerweger 06/2005,
% facet orientation re-worked by Florian Hollerweger, 02/2006

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ----- INPUT ARGUMENT CHECKING AND PROCESSING, CONSTANT DEFINITIONS -----

% Check number of input arguments
if nargin < 1
    error('Not enough input arguments.')
elseif nargin > 2
    error('Too many input arguments.')    
end


% Check 'shape' input argument
% Safety
shape = lower(shape);
if ~strcmp(shape,'tetrahedron') && ~strcmp(shape,'tetra') && ...
        ~strcmp(shape,'hexahedron') && ~strcmp(shape,'hex') && ...
        ~strcmp(shape,'cube') && ~strcmp(shape,'octahedron') && ...
        ~strcmp(shape,'oct') && ~strcmp(shape,'dodecahedron') && ...
        ~strcmp(shape,'dodec') && ~strcmp(shape,'icosahedron') && ...
        ~strcmp(shape,'ico') 
    error('Unknown ''shape'' input argument.');
end


% Check 'radius' input argument
if  ~exist('radius','var') || isempty(radius)
    radius = 1;
elseif ~isnumeric(radius) || ~isscalar(radius) || radius <= 0
    error('Unknown ''radius'' input argument. Must be positive scalar numeric value.');
end


% ----------------------------- CALCULATIONS -----------------------------

switch shape
    
    case { 'tetrahedron', 'tetra' }
        
        polyhedron.vertices  =  [  1     1     1
                                  -1    -1     1
                                  -1     1    -1
                                   1    -1    -1 ] * (sqrt(3)/3) * radius;

        polyhedron.faces     =  [ 1    2    3
                                  1    4    2
                                  3    2    4
                                  4    1    3 ];


    case {'hexahedron', 'hex', 'cube'}
        
        polyhedron.vertices  =  [  1     1     1
                                   1     1    -1
                                   1    -1     1
                                   1    -1    -1
                                  -1     1     1
                                  -1     1    -1
                                  -1    -1     1
                                  -1    -1    -1 ] * (sqrt(3)/3) * radius;

        polyhedron.faces     =  [ 2    4    3    1
                                  8    6    5    7
                                  6    2    1    5
                                  4    8    7    3
                                  3    7    5    1
                                  6    8    4    2];


    case { 'octahedron', 'oct' }
        
        polyhedron.vertices  =  [  1     0     0
                                  -1     0     0
                                   0     1     0
                                   0    -1     0
                                   0     0     1
                                   0     0    -1 ] * radius;

        polyhedron.faces     =  [ 1    5    3
                                  3    5    2
                                  2    5    4
                                  4    5    1
                                  1    3    6
                                  3    2    6
                                  2    4    6
                                  4    1    6 ];


    case { 'dodecahedron', 'dodec' }
        
        % Golden mean
        gm = (sqrt(5)+1)/2;
        
        polyhedron.vertices  =  [     0     1/gm       -gm
                                      1        1        -1
                                   1/gm       gm         0
                                     -1        1        -1
                                  -1/gm       gm         0
                                    -gm        0     -1/gm
                                     -1        1         1
                                    -gm        0      1/gm
                                     -1       -1         1
                                      0    -1/gm        gm
                                   1/gm      -gm         0
                                      1       -1         1
                                     gm        0      1/gm
                                      1       -1        -1
                                     gm        0     -1/gm
                                  -1/gm      -gm         0
                                      0    -1/gm       -gm
                                     -1       -1        -1
                                      1        1         1
                                      0     1/gm        gm ] / sqrt(3) * radius;
                                  
        polyhedron.faces     =  [  2     3     5     4     1
                                  17    14    15     2     1
                                  18    16    11    14    17           
                                   1     4     6    18    17
                                  15    13    19     3     2
                                  14    11    12    13    15
                                  13    12    10    20    19
                                   3    19    20     7     5
                                   4     5     7     8     6
                                  10     9     8     7    20
                                  18     6     8     9    16
                                  16     9    10    12    11 ];


    case { 'icosahedron', 'ico' }
        
        % Golden mean
        gm  = (sqrt(5)+1) / 2;
        tau = gm / sqrt(1 + gm^2);
        one = 1 / sqrt(1 + gm^2);
        
        polyhedron.vertices  =  [  tau     one       0
                                 -tau     one       0
                                 -tau    -one       0
                                  tau    -one       0
                                  one       0     tau
                                  one       0    -tau
                                 -one       0    -tau
                                 -one       0     tau
                                    0     tau     one
                                    0    -tau     one
                                    0    -tau    -one
                                    0     tau    -one ] * radius;

        polyhedron.faces     =  [  5     8     9
                                   5    10     8
                                   6    12     7
                                   6     7    11
                                   1     4     5
                                   1     6     4
                                   3     2     8
                                   3     7     2
                                   9    12     1
                                   9     2    12
                                  10     4    11
                                  10    11     3
                                   9     1     5
                                  12     6     1
                                   5     4    10
                                   6    11     4
                                   8     2     9
                                   7    12     2
                                   8    10     3
                                   7     3    11 ];


end


return