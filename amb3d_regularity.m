function [regularity,condNum,regularityMtx] = amb3d_regularity(order,speakerDirection)

%3LD :: AMB3D_REGULARITY Evaluate Ambisonic regularity of a loudspeaker layout.
%   [REGULARITY, CONDNUM, C] = AMB3D_REGULARITY(M,SPK) returns a string
%   REGULARITY, which specifies whether a 3D layout of L loudspeakers is
%   'regular', 'semiregular' or 'irregular' in the Ambisonic sense for a
%   specific Ambisonic order M, as defined on page 176 in [1]. It also
%   returns the condition number CONDNUM of the re-encoding matrix C, which
%   can be built from the loudspeaker position information and is also a
%   regularity criterion according to p.39 in [2]. The third output
%   argument C is the matrix (C*C')/L, which is used to evaluate the
%   regularity. The layout is regular if this matrix is the unity matrix,
%   and semi-regular if it is a diagonal matrix. Note that slight
%   variations have been allowed in order to account for numerical
%   inaccuracies. SPK is a L-by-2 matrix containing the azimuths (first
%   column) and elevations (second column) of the L loudspeakers. The same
%   spherical coordinate system as in Matlab's SPH2CART is applied.
%   Note that as usual, the N3D encoding convention is applied in
%   evaluating the regularity and condition number of the layout (see p.175
%   in [1]).
%
%   Example:
%   A dodecahedron is regular for second order Ambisonic
%       p = platonicsolid('dodec');
%       [az elev] = cart3sph(p.vertices);
%       amb3d_regularity(2,[az elev])
%
%   See also: AMB3D_ENCODER (3LD), AMB3D_DECODER (3LD).
%
%   Reference:
%     [1] J. Daniel, "Repr�sentation de champs acoustiques, applications a
%         la transmission et a la reproduction de sc�nes sonores complexes
%         dans un contexte multim�dia", PhD thesis, University of Paris 6,
%         2000
%     [2] A. Sontacchi, "Dreidimensionale Schallfeldreproduktion f�r
%         Lautsprecher und Kopfh�reranwendungen", PhD thesis, Institute
%         of Electronic Music and Acoustics, Graz, Austria, 2003


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ----- INPUT ARGUMENT CHECKING AND PROCESSING, CONSTANT DEFINITIONS -----

% Check number of input arguments
if nargin < 2
    error('Not enough input arguments.')
elseif nargin > 2
    error('Too many input arguments.')    
end


% Check 'order' input argument
if ~isnumeric(order) || ~isscalar(order) || order < 0
    error('''Invalid ''order'' input argument. Must be non-negative integer.');
end

% Number of Ambisonic channels N for the given order M: N = (M+1)^2
numChannels = (order + 1)^2;


% Check 'speakerDirection' input argument
if size(speakerDirection,2) ~= 2
    error('''Invalid ''speakerDirection'' input argument. Must be numeric L-by-2 array');
end

% Number of loudspeakers
numSpeakers = size(speakerDirection,1);


% ----------------------------- CALCULATIONS -----------------------------

% Calculate the re-encoding matrix C
reEncodingMtx = amb3d_encoder(order,speakerDirection);


% Here, we are only interested in the gain values returned by the encoder
reEncodingMtx = reEncodingMtx.gain;


% We'll use the following matrix matrix to evaluate the regularity
regularityMtx = (reEncodingMtx * reEncodingMtx') / numSpeakers;

if all( all( abs(regularityMtx - eye(numChannels)) < 0.01 ) )
    % Layout is regular if (C*C')/L is the identity matrix
    regularity = 'regular';

elseif all( all( abs((regularityMtx~=0) - eye(numChannels)) < 0.01 ) )
    % Layout is semiregular if (C*C')/L is a diagonal matrix
    regularity = 'semiregular';

else
    % Otherwise, layout is irregular
    regularity = 'irregular';

end


% Condition number of the C matrix
if nargout > 1
    condNum = cond(reEncodingMtx);
end


return