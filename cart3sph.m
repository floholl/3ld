function [azimuth,elevation,radius] = cart3sph(cartesian)

%3LD :: [AZ,ELEV,R] = CART3SPH(XYZ) Matrix conversion from cartesian to spherical coordinates.
%   This function is identical to Matlab's CART2SPH, but takes a single
%   N-by-3 matrix as an input argument, rather than three separate arrays.
%   The columns of XYZ represent the x, y, and z components of N different
%   points. The function returns the AZ (azimuth), ELEV (elevation), and R
%   (radius) components as three vectors. This is convenient to evaluate
%   the spherical coordinates of a vertices array as returned by
%   PLATONICSOLID, GEOSPHERE, or BUCKY2 (all 3LD) without prior separation
%   into x,y,z vectors.
%
%   See also: SPH3CART (3LD).


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ------------------------ INPUT ARGUMENT CHECKING ------------------------

% Check number of input arguments
if nargin < 1
    error('Not enough input arguments.')
elseif nargin > 1
    error('Too many input arguments.')    
end

% Check 'cartesian' input argument
if ~isnumeric(cartesian) || size(cartesian,2) ~= 3
    error('Invalid ''cartesian'' input argument. Must be numeric N-by-3 array.');
end


% ----------------------------- CALCULATIONS -----------------------------

% Get cartesian components
x = cartesian(:,1);
y = cartesian(:,2);
z = cartesian(:,3);

% Convert to spherical components
azimuth   = atan2(y , x);
elevation = atan2(z , sqrt(x.^2 + y.^2));
radius    = sqrt(x.^2 + y.^2 + z.^2);

return