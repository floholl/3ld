function hdl = plot3LD(thingToPlot,lockStatus)

%3LD :: PLOT3LD Plot polyhedra and spherical functions.
%   H = PLOT3LD(THING[,LOCK]) is a straightforward-to-use plotter for
%   vertices/faces structures representing loudspeaker layouts and
%   spherical functions representing radius or loudspeaker density
%   functions.
%
%   In the case of loudspeaker layouts, THING is a structure with fields
%   'vertices' and 'faces'. Additionally, you can specify the LOCK status
%   of the speakers (used also in MINENERGYCONF), which will be represented
%   with the color of the loudspeaker index in the plot: IDs of unlocked
%   speakers are black, whereas locked loudspeaker indices appear red. The
%   plotting is done by Matlab's PATCH function. 3LD functions which return
%   a structure that can be directly plotted with PLOT3LD are
%   PLATONICSOLID, BUCKY2, and GEOSPHERE.
%
%   For plotting spherical functions, THING is a function handle depending
%   on two variables, the first of which is interpreted as the azimuth and
%   the second one of which as the elevation. The plotting is done by 3LD's
%   EZSPHERICAL. The output of 3LD's HANDLESPHARM can be plotted directly
%   using PLOT3LD.
%
%   See also: EZSPHERICAL (3LD), PLATONICSOLID (3LD), BUCKY2 (3LD),
%             GEOSPHERE (3LD), PATCH.


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ------------------------ INPUT ARGUMENT CHECKING ------------------------

% Check number of input arguments
if nargin < 1
    error('Not enough input arguments')
elseif nargin > 2
    error('Too many input arguments')    
end

% Check 'thingToPlot' input argument
if isstruct(thingToPlot) && isfield(thingToPlot,'vertices') && isfield(thingToPlot,'faces')
    mode = 'layout';
    % Check 'lockStatus' input argument
    if ~exist('lockStatus','var') || isempty(lockStatus)
        % Default to unlocked speakers if not specified
        lockStatus = zeros(size(thingToPlot.vertices,1),1);
    end
elseif isa(thingToPlot,'function_handle')
    mode = 'function';  
else
    error('Invalid ''thingToPlot'' input argument. Must be structure or function handle.');
end


% ----------------------------- CALCULATIONS -----------------------------

% Create new figure
figure;

switch mode
    
    case 'layout'
        % Plot parameters
        facecolor = [.1 .7 .3];
        facealpha = 0.8;
        edgecolor = [.8 .8 .8];
        
        % Plot the loudspeaker layout
        hdl = patch('faces',thingToPlot.faces,'vertices',thingToPlot.vertices,'facecolor',facecolor,'facealpha',facealpha,'edgecolor',edgecolor);
        
        % Add additional text information about the layout
        numSpk = size(thingToPlot.vertices,1);
        numFac = size(thingToPlot.faces,1);
        title({numSpk 'speakers,' numFac 'faces'});
        set(gcf,'Name','Polyhedron','NumberTitle','off');
    
        % Enter speaker IDs as text objects at according positions.
        for j = 1:numSpk
            % IDs of locked speakers are drawn red, the other ones black
            text(thingToPlot.vertices(j,1),thingToPlot.vertices(j,2),thingToPlot.vertices(j,3), int2str(j),'Color',[lockStatus(j),0,0]);
        end

    case 'function'
        hdl = ezspherical(thingToPlot,500);
        set(gcf,'Name','Spherical Function','NumberTitle','off');
        set(hdl,'EdgeColor','none','EdgeLighting','none');
     
end

camlight('headlight','infinite');
light
lighting gouraud;   % gouraud, flat, phong, gouraud, none
axis equal;
axis vis3d;
material dull;      % dull, shiny, metal
view(125,30);
grid off;
rotate3d on;
xlabel('x'), ylabel('y'),zlabel('z');
zoom(0.8);

return