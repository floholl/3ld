function [squaredPressureError,amplitudeError] = pressure_errors(referenceField,synthesizedField)

%3LD :: PRESSURE_ERRORS complex sound pressure reconstruction errors.
%   [PE2,AE] = PRESSURE_ERRORS(REF,SYNTH) computes errors among two
%   complex sound pressure fields REF and SYNTH, where the first represents
%   the original soundfield, and the latter represents the reconstructed
%   soundfield. The size of the input arrays is arbitrary, but has to
%   match. The error is calculated as a scalar field with the size of the
%   two input fields.
%
%   PE2 is the 'squared sound pressure error', i.e. the squared difference
%   of the two fields. Note that it is complex. AE is the 'sound pressure
%   amplitude error', which is the absolute value of the difference of the
%   two fields and is real.
%
%   See also: SOUNDFIELDER (3LD), DIRECTION_DEVIATION (3LD).


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger (floholl_AT_sbox.tugraz.at) and
% (c) 2003 Alois Sontacchi.
% This function bases on Matlab code originally written by Alois
% Sontacchi at the Institute of Electronic Music and Acoustics (IEM) at
% Graz, Austria.

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ------------------------ INPUT ARGUMENT CHECKING ------------------------

% Check number of input arguments
if nargin < 2
    error('Not enough input arguments')
elseif nargin > 2
    error('Too many input arguments')    
end

% Check input array sizes
if ~isequal(size(referenceField),size(synthesizedField))
    error('Dimensions of input arrays must be identical.');
end


% ----------------------------- CALCULATIONS -----------------------------

% Calculate the squared sound pressure error
squaredPressureError = (referenceField - synthesizedField).^2;

if nargout > 1
    % Calculate the absolute sound pressure amplitude error
    amplitudeError = abs( referenceField - synthesizedField );
end

return