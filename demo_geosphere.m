%3LD :: DEMO_GEOSPHERE Bucky ball geodesic extension.
%   This demo shows the flexibility of the FREQ input argument in the
%   GEOSPHERE function, allowing for different geodesic frequencies in each
%   iteration and for each facet shape of a geodesic extension process.
%   First, a bucky ball is created, using 3LD's BUCKY2. Then, one of its
%   pentagonal facets is replaced by a triangle and a rectangle.
%   Eventually, the geodesic extension is applied in two iterations. In the
%   first iteration, the triangles are tessellated at f=2, the rectangles
%   at f=3, and pentagons and hexagons are midpoint-triangulated. In the
%   second iteration, triangles are ignored, rectangles are
%   midpoint-triangulated, and no other shapes remain for tessellation.
%
%   If the demo seems not to work, try to clear variables in your workspace
%   ('clear all').
%
%   See also: GEOSPHERE (3LD), BUCKY2 (3LD).


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% --------------------------------- DEMO ---------------------------------p = bucky2;

p = bucky2;

p.faces(1,:) = [];
p.faces(end+1:end+2,:) = [3 5 4 NaN NaN NaN ; 3 2 1 5 NaN NaN];

p = geosphere(p,[2 0; 3 1; 1 4; 7 1]);
plot3LD(p); view(130,90), zoom(1.9)