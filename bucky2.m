function buckyBall = bucky2(radius)

%3LD :: BUCKY2 Vertices/faces structure of the bucky ball.
%   B = BUCKY2([RADIUS]) generates the vertices/faces structure of a
%   truncated icosahedron, also refered to as 'bucky ball'. This function
%   uses Matlab's BUCKY, but replaces its adjacency matrix with a faces
%   matrix. The output structure B has fields 'vertices', which is a V-by-3
%   array specifying the x,y,z coordinates as returned by BUCKY, and a
%   F-by-6 array with rows containing the row indices of the vertices
%   forming a facet. Note that the Bucky ball consists of hexagons and
%   pentagons. For the rows in the facet matrix representing a pentagon,
%   the last entry is NaN. Facets are left-oriented as seen from the origin
%   of the bucky ball. The output structure B can be plotted directly using
%   Matlab's PATCH or 3LD's PLOT3LD.
%
%   See also: BUCKY.


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% --------------------------------- NOTES ---------------------------------

%   Possible improvements for future versions:
%   - Plotting BUCKY2 gives weird connections from vertex 1 to vertices
%     10,15,20,25,30,35,40,45,50,55,60. Find out why.


% ------------------------ INPUT ARGUMENT CHECKING ------------------------

% Check number of input arguments
if nargin > 1
    error('Too many input arguments.')    
end

% Check 'radius' input argument
if ~exist('radius','var') || isempty(radius)
    % Default to 1 if not specified
    radius = 1;
end

% ----------------------------- CALCULATIONS -----------------------------

% Get vertex coordinates from 'bucky'. The adjacency matrix 'A' will not be used
[A,buckyBall.vertices] = bucky;


% Push the vertices to the specified radius
[az elev] = cart2sph(buckyBall.vertices(:,1), buckyBall.vertices(:,2), buckyBall.vertices(:,3));
r = radius;
[buckyBall.vertices(:,1), buckyBall.vertices(:,2), buckyBall.vertices(:,3)] = sph2cart(az,elev,r);


% Build faces matrix with vertex indices (12 pentagons, 20 hexagons)
buckyBall.faces = [    5 4 3 2 1 NaN
                     10 9 8 7 6 NaN
                     15 14 13 12 11 NaN
                     20 19 18 17 16 NaN
                     25 24 23 22 21 NaN
                     30 29 28 27 26 NaN
                     35 34 33 32 31 NaN
                     40 39 38 37 36 NaN
                     45 44 43 42 41 NaN
                     50 49 48 47 46 NaN
                     55 54 53 52 51 NaN
                     60 59 58 57 56 NaN      
                     43 44 46 47 28  29
                     48 49 51 52 23  24
                     53 54 31 32 18  19
                     33 34 36 37 13  14
                     38 39 41 42  8   9
                     26 27 25 21  4   5
                     21 22 20 16  3   4
                     16 17 15 11  2   3
                     11 12 10  6  1   2
                      6  7 30 26  5   1
                     18 32 33 14 15  17
                     13 37 38  9 10  12
                      8 42 43 29 30   7
                     28 47 48 24 25  27
                     23 52 53 19 20  22
                     35 56 57 40 36  34
                     40 57 58 45 41  39
                     45 58 59 50 46  44
                     50 59 60 55 51  49
                     55 60 56 35 31  54    ];

    
return