function Y = solospharm(degree,orderTimesSigma,azimuth,elevation,normalize)

%3LD :: SOLOSPHARM Single spherical harmonic function.
%   Y = SOLOSPHARM(N,M_TIMES_SIG,AZ,ELEV[,NORM]) computes the spherical
%   harmonic functions of degree N, order M, and superscript SIG = +-1. The
%   functions are evaluated for each element of AZ and ELEV. N must be a
%   scalar integer. M_TIMES_SIG must be a vector with each element i
%   fullfilling the condition abs(M_TIMES_SIG(i)) <= N. AZ and ELEV must be
%   arrays of identical size containing, the azimuth and elevation
%   arguments in radians. NORM is an optional argument, specifying
%   different normalizations of the Legendre polynomials, which are
%   included in the spherical harmonics. Legal terms are 'unnorm','sch' or
%   'norm', and default is 'unnorm'. Y returns the values of the spherical
%   harmonic functions for each element in the M_TIMES_SIG vector and each
%   pair AZ, ELEV. The first dimension of Y refers to the different
%   spherical harmonic functions, whereas the other dimensions refer to
%   those of the input arrays AZ and ELEV. If M_TIMES_SIG is a scalar, the
%   first (singleton) dimension is removed, and Y has the same size as AZ
%   and ELEV.
%
%   SOLOSPHARM calls SPHARMONIC (3LD), which calls LEGENDRE with the NORM
%   argument; more information on the normalization options can be found in
%   LEGENDRE. SPHARMONIC always returns the spherical harmonic functions of
%   all orders and both superscripts for the specified degree N, i.e. from
%   M_TIMES_SIG = -N:N. To be able to access single harmonic function of a
%   certain order, SOLOSPHARM has been introduced. Note that it is 
%   inefficient, since SOLOSPHARM simply throws away the functions returned
%   by SPHARMONIC which have not been requested. However, SPHARMONIC uses
%   the native Matlab function LEGENDRE, which does not return functions of
%   a single order either.
%
%   See also: SPHARMONIC (3LD), LEGENDRE.


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ----- INPUT ARGUMENT CHECKING AND PROCESSING, CONSTANT DEFINITIONS -----

% Check number of input arguments
if nargin < 4
    error('Not enough input arguments')
elseif nargin > 5
    error('Too many input arguments')    
end

% Check 'orderTimesSigma' input argument
if any(abs(orderTimesSigma) > degree)
    error('Illegal entries in ''orderTimesSigma'' input argument. Must be < degree.');
end

% Check 'normalize' input argument
if  ~exist('normalize','var') || isempty(normalize)
    % Default to 'unnorm' if not specified
    normalize = 'unnorm';
end

% Save for later restoration of original dimensions
sizeAz = size(azimuth);

% Convert to vectors for internal calculations
azimuth = azimuth(:);
elevation = elevation(:);


% ----------------------------- CALCULATIONS -----------------------------

% Call spharmonic to calculate all orders for the degree specified
Y = spharmonic(degree,azimuth,elevation,normalize);


% The first dimension of the array returned by SPHARMONIC refers to the
% order of the spherical harmonic function from -degree:degree, so the
% index for the first dimension of Y(degree,order) equals
% degree+order+1, e.g. for degree = 2:
%
% order | index=degree+order+1
% -----------------------------
%  -2   |        1
%  -1   |        2
%   0   |        3
%   1   |        4
%   2   |        5
Y = Y(degree + orderTimesSigma + 1 , :);


% Restore original dimensions
Y = reshape(Y,[size(Y,1) sizeAz]);


% Remove singleton dimensions
Y = squeeze(Y);


return