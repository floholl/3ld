function vertices = rotate_xyz(vertices,axis,angle)

%3LD :: ROTATE_XYZ Rotate xyz coordinates.
%   V = ROTATE_XYZ(COORD,AXIS,ANGLE) rotates the P-by-3 array COORD, which
%   represents the x,y,z coordinates of P points, around either AXIS
%   (specified with strings 'x', 'y', or 'z') by an ANGLE given in radians
%   The same coordinate system as in Matlab's SPH2CART is applied.
%   Arbitrary rotation axes can be achieved by subsequent application of
%   ROTATE_XYZ.
%
%   Example:
%   Rotate an octahedron 45 degrees around the x axis
%       p = platonicsolid('oct');
%       p.vertices = rotate_xyz(p.vertices,'x',pi/4);
%
%   See also: PLATONICSOLID (3LD), GEOSPHERE (3LD), BUCKY2 (3LD),
%             MINENERGYCONF (3LD).


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ----- INPUT ARGUMENT CHECKING AND PROCESSING, CONSTANT DEFINITIONS -----

% Check number of input arguments
if nargin < 3
    error('Not enough input arguments.')
elseif nargin > 3
    error('Too many input arguments.')    
end

% Check 'vertices' input argument
if ~isnumeric(vertices)
    error('Invalid ''vertices'' input argument. Must be numeric.');
elseif size(vertices,2) ~= 3
    error('Invalid ''vertices'' input argument. Must be N-by-3 array.');
end

% Check 'axis' input argument
if ~isscalar(axis)
    error('Invalid ''axis'' input argument. Must be scalar.');
elseif ~strcmp(axis,'x') && ~strcmp(axis,'y') && ~strcmp(axis,'z')
    axis = lower(axis); % Safety
    error('Invalid ''axis'' input argument. Must be ''x'',''y'' or ''z''.');
end

% Check 'angle' input argument
if ~isnumeric(angle) || ~isscalar(angle)
    error('Invalid ''angle'' input argument. Must be numeric scalar.');
end


% ----------------------------- CALCULATIONS -----------------------------

% Build the required rotation matrix
switch axis
    
    case 'x'
        rotationMatrix = [1 0 0; 0 cos(angle) -sin(angle); 0 sin(angle) cos(angle)];
        
    case 'y'
        rotationMatrix = [cos(angle) 0 -sin(angle) ; 0 1 0 ; sin(angle) 0 cos(angle)];
        
    case 'z'
        rotationMatrix = [cos(angle) -sin(angle) 0;  sin(angle) cos(angle) 0;  0 0 1];
            
end


% Do the actual rotation
vertices = vertices';
vertices = [rotationMatrix * vertices]';


return