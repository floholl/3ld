function speakerGain = amb3d_decoder(B,speakerDirection,order,method,flavor)

%3LD :: AMB3D_DECODER Ambisonic decoded loudspeaker gains.
%   G = AMB3D_DECODER(B,SPK[,M,METHOD,FLAVOR]) derives the loudspeaker
%   gains for an array of L speakers from the Ambisonic channel gains of a
%   3D encoded soundfield.
%
%   B, which typically represents the output of 3LD's AMB3D_ENCODER, has to
%   be a structure with fields 'gain', and 'sort'. 'gain' is an
%   N-by-S array, with rows representing the N Ambisonic channels of S
%   sound sources represented in the columns. 'sort' determines the order in
%   which the Ambisonic channels appear. Check the documentation of
%   AMB3D_ENCODER for a description.
%
%   SPK specifies the directions of the loudspeakers. It must be an L-by-2
%   array, representing azimuth and elevation in radians. The same
%   left-oriented coordinate system as in Matlab's SPH2CART is applied.
%   Note that AMB3D_DECODER calls AMB3D_ENCODER for re-encoding the
%   loudspeaker layout. Thus, the N3D encoding convention [1] is applied in
%   this process
%
%   M is the Ambisonic order at which the decoder operates. If not
%   specified, it is derived from the encoded input material B. Otherwise,
%   the order of the input material can only be overridden with smaller
%   values.
%
%   METHOD specifies the decoding method. Legal strings are 'projection'
%   (or 'proj') and 'pseudoinverse' (or 'pinv').
%
%   FLAVOR specifies the decoder flavor. Legal strings are 'basic' and
%   'inphase'.
%
%   G is an L-by-S matrix. Each column represents the loudspeaker gains
%   required to reproduce one of the S independent virtual sound sources,
%   which are represented in the rows. Note that the function does by no
%   means normalize the output gains. It is your responsibility to avoid
%   clipping.
%
%   Example:
%   Decode the output of the example in the AMB3D_ENCODER documentation to
%   an icosahedron loudspeaker layout at second Ambisonic order.
%       p = platonicsolid('ico');
%       [az elev] = cart3sph(p.vertices);
%       g = amb3d_decoder(B,[az elev],2,'pinv','inphase')
%
%   See also: AMB3D_ENCODER (3LD), AMB3D_REGULARITY (3LD), VBP (3LD),
%             SPHARMONIC (3LD).
%   
%   Reference:
%     [1] J. Daniel, "Représentation de champs acoustiques, applications a
%         la transmission et a la reproduction de scènes sonores complexes
%         dans un contexte multimédia", PhD thesis, University of Paris 6,
%         2000


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% --------------------------------- NOTES ---------------------------------

% Abbreviations used in the comments:
% M ... Ambisonic order, N ... number of Ambisonic channels,
% L ... number of loudspeakers, B ... encoded Ambisonic channels
% C ... re-encoding matrix, D ... decoding matrix,
% p ... decoded loudspeaker signals


% ----- INPUT ARGUMENT CHECKING AND PROCESSING, CONSTANT DEFINITIONS -----

% Check number of input arguments
if nargin < 2
    error('Not enough input arguments.')
elseif nargin > 5
    error('Too many input arguments.')    
end


% Check 'B' input argument
if ~isfield(B,'gain') || ~isfield(B,'sort')
    error('''B'' input argument is missing fields ''gain'' and/or ''sort''.');
end
% Derive the Ambisonic order of the input material from its number of
% channels: M = sqrt(N) - 1
inputOrder = sqrt(size(B.gain,1)) - 1;


% Check 'speakerDirection' input argument
if size(speakerDirection,2) ~= 2
    error('Illegal dimensions of ''speakerDirection'' input array. Must be numSpeakers-by-2 array.');
end
% Number of loudspeakers
numSpeakers = size(speakerDirection,1);


% Check 'order' input argument
if  ~exist('order','var') || isempty(order)
    % Default to input order if not specified
    order = inputOrder;
elseif ~isnumeric(order) || ~isscalar(order) || order < 0
    error('''Unknown ''order'' input argument. Must be non-negative integer.');
elseif order > inputOrder
    order = inputOrder;
    warning('Ambisonic order degraded to match the order of the input material.');
else
    % The we're fine with the specified order  
end
% Calculate the number of Ambisonic channels for the decoding order:
% N = (M+1)^2
numChannels = (order + 1)^2;
B.gain = B.gain(1:numChannels,:);


% Check whether there are more Ambisonic channels than loudspeakers
if numSpeakers < numChannels
    warning(['For a decoding order of ' num2str(order) ', the number of Ambisonic channels exceeds the number of loudspeakers.']);
end

% Check 'method' input argument
if  ~exist('method','var') || isempty(method)
    % Default to pseudoinverse decoding method if not specified
    method = 'pseudoinverse';
else
    % Safety
    method = lower(method);
    if strcmp(method,'pseudoinverse') || strcmp(method,'pinv')
        method = 'pseudoinverse';
    elseif strcmp(method,'projection') || strcmp(method,'proj')
        method = 'projection';
    else
        error('Unknown decoding method input argument. Must be ''pseudoinverse''/''pinv'' or ''projection''/''proj''.');
    end
end


% Check 'flavor' input argument
if  ~exist('flavor','var') || isempty(flavor)
    % Default to basic decoder flavor if not specified
    flavor = 'basic';
else
    % Safety
    flavor = lower(flavor);
    if ~strcmp(flavor,'basic') && ~strcmp(flavor,'inphase')
        error('Unknown decoder flavor input argument. Must be ''basic'' or ''inphase''.');
    end
end



% ----------------------------- CALCULATIONS -----------------------------

% Re-encode loudspeaker layout into the re-encoding matrix C
reEncodingMtx = amb3d_encoder(order,speakerDirection,[],0,B.sort);

% Here, we are only interested in the 'gain' field
reEncodingMtx = reEncodingMtx.gain;


% Build decoding matrix D according to specified decoding method
if strcmp(method,'pseudoinverse')
    % Derive decoding matrix through pseudoinverse: D = pinv(C)
    decodingMtx = pinv(reEncodingMtx);
    
elseif strcmp(method,'projection')
    % Derive decoding matrix through projection: D = 1/L * C'
    decodingMtx = reEncodingMtx' / numSpeakers;
    
end


% Modify decoding matrix according to the specified decoding flavor
if strcmp(flavor,'inphase')
    
    % Build vector 'orderOfChannels', containing the order of each Ambisonic channel
    % Initialize with W channel element
    orderOfChannels = 0;
    
    % TODO: FOR-LOOP COULD BE AVOIDED IN A FUTURE VERSION
    for m = 1:order
        orderOfChannels(end+1 : end+2*m+1) = m;
    end
    
    % Calculate inphase weighting factors and apply them to the decoding
    % matrix: D = D * diag(inphase)
    inphaseFactors = factorial(order) .* factorial(order+1) ./ ( factorial(order+orderOfChannels+1) .* factorial(order - orderOfChannels) );
    decodingMtx = decodingMtx * diag(inphaseFactors);
    
end


% Actual decoding process: p = D * B
speakerGain = decodingMtx * B.gain;


% Some verbose
disp(['... decoding Ambisonic soundfield at order ' num2str(order)]);


return
