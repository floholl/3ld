%3LD :: DEMO_MINENERGY Plot the process of a minimal energy configuration.
%   This demo shows the process of electrons on a spherical surface
%   distributing themselves from their random initial positions towards a
%   minimal energy configuration, in which they are 'evenly' distributed on
%   the sphere.
%
%   If the demo seems not to work, try to clear variables in your workspace
%   ('clear all').
%
%   See also: MINENERGYCONF (3LD).


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ---------------------------- USER PARAMETERS ----------------------------

numElectrons = 50;
numIterations = 25;


% --------------------------------- DEMO ---------------------------------

% Build initial (random) minimal energy configuration (zero iterations)
p.vertices = minenergyconf(numElectrons,0);

% Plot the configuration
h = plot3(p.vertices(:,1), p.vertices(:,2), p.vertices(:,3),'o');
grid on, axis equal, rotate3d on

% Add iteration number as plot title
t = title({'Iteration number ' 0},'Position',[0 0 1.7],'FontWeight','bold');

% Refine initial configuration and plot each iteration of the process
for i = 1:numIterations
   drawnow;
   p.vertices = minenergyconf(p.vertices,1);
   set(h,'XData',p.vertices(:,1),'YData',p.vertices(:,2),'ZData',p.vertices(:,3));
   set(t,'String',{'Iteration number ' i});
end