function radians = deg2rad(degrees)

%3LD :: RAD = DEG2RAD(DEG) Convert degrees to radians.
%   Convert an arbitrary numeric input array DEG from degrees to radians.
%
%   See also: RAD2DEG (3LD).


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ------------------------ INPUT ARGUMENT CHECKING ------------------------

% Check number of input arguments
if nargin < 1
    error('Not enough input arguments.')
elseif nargin > 1
    error('Too many input arguments.')    
end

% Check 'degrees' input argument
if ~isnumeric(degrees)
    error('Invalid ''degrees'' input argument. Must be numeric.');
end


% ----------------------------- CALCULATIONS -----------------------------

radians = (degrees .* 2 .* pi) ./ 360;

return