function vertices = map_to_surface(vertices,radius)

%3LD :: MAP_TO_SURFACE Map vertices of a polyhedron to a surface.
%   S = MAP_TO_SURFACE(V[,RADIUS]) maps the N vertices of a polyhedron - 
%   represented by the N-by-3 array V which contains their x,y,z
%   coordinates - to a surface defined by a function handle RADIUS, which
%   represents the radius as a spherical function. The first argument of
%   the function handle is interpreted as the azimuth, and the second as
%   the elevation. RADIUS can also be a scalar, in which case the vertices
%   are mapped to a sphere of that radius. If RADIUS is not specified, the
%   vertices are mapped to the unit sphere of radius 1. The N-by-3 output
%   array represents the new x,y,z coordinates of the vertices. Only the
%   radius of the vertices will be affected, whereas their direction is
%   maintained. The same cartesian and spherical coordinate systems as in
%   Matlab's SPH2CART are applied.
%
%   Example:
%   Create a geodesic sphere and map its vertices to a surface derived from
%   a spherical harmonic function, using 3LD functions. Plot the original
%   polyhedron, the radius function and the new polyhedron.
%       p = geosphere('ico',[2,2],1);
%       patch(p,'facecolor',[.1 .7 .3],'facealpha',0.8);
%       radius = handlespharm('abs(3*Y(7,-5))+1');
%       figure; ezspherical(radius,200);
%       p.vertices = map_to_surface(p.vertices,radius);
%       figure; patch(p,'facecolor',[.1 .7 .3],'facealpha',0.8);
%
%   See also: HANDLESPHARM (3LD), EZSPHERICAL (3LD), 
%             CALIBRATE_LAYOUT (3LD).


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ------------------------ INPUT ARGUMENT CHECKING ------------------------

% Check number of input arguments
if nargin < 1
    error('Not enough input arguments.')
elseif nargin > 2
    error('Too many input arguments.')    
end

% Check 'vertices' input argument
if ~isequal(ndims(vertices),2) || ~isequal(size(vertices,2),3)
    error('Invalid ''vertices'' input argument. Must be N-by-3 array.');
end

% Check 'radius' input argument
if  ~exist('radius','var') || isempty(radius)
    % Default to constant radius of 1 if not specified
    radiusFct = @(az,elev) 1;
elseif isnumeric(radius) && isscalar(radius)
    % If radius is a scalar, assume spherical surface
    radiusFct = @(az,elev) radius;
elseif isa(radius,'function_handle')
    % If radius already is a function handle, simply use that one
    radiusFct = radius;
else
    error('Unknown ''radius'' input argument. Must be a scalar numeric value or function handle.');
end


% ----------------------------- CALCULATIONS -----------------------------

% Get azimuth and elevation of all vertices
[az,elev] = cart2sph(vertices(:,1), vertices(:,2), vertices(:,3));

% Calculate radius according to input argument
r = radiusFct(az,elev);
r = r(:);       % Make it a column vector

% Apply radii according to the surface function
[vertices(:,1),vertices(:,2),vertices(:,3)] = sph2cart(az,elev,r);

return