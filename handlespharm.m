function h = handlespharm(string)

%3LD :: HANDLESPHARM Handle to spherical harmonic function.
%   H = HANDLESPHARM(STRING) returns a handle to a combination of spherical
%   harmonic functions. STRING is a string defining this combination using
%   terms 'Y(N,M*SIG)', where N refers to the degree, M to the order, and
%   SIG = +-1 to the superscript of the spherical harmonic function. Note
%   that abs(M*SIG) <= N. The output of HANDLESPHARM can be plotted
%   directly with EZSPHERICAL (3LD). Note that HANDLESPHARM the spherical
%   harmonics in HANDLESPHARM are always Schmidt-seminormalized.
%
%   HANDLESPHARM calls SOLOSPHARM (3LD), which calls SPHARMONIC (3LD),
%   which calls Matlab's LEGENDRE. Refer to those functions for more
%   information.
%
%   Examples:
%
%       [1] Plot the sum of the spherical harmonic of degree 3, order 2, and
%       superscript +1, and the absolute value of the function of degree 7,
%       order 1, and superscript -1:
%       h = handlespharm('Y(3,2) + abs(Y(7,-1))');
%       ezspherical(h);
%
%       [2] Butterfly demo
%       h = handlespharm('(Y(4,3)*Y(5,5)) / abs(Y(1,0)+2)');
%       ezspherical(h);
%
%   See also: SPHARMONIC (3LD), SOLOSPHARM(3LD), EZSPHERICAL (3LD),
%             LEGENDRE.


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% --------------------------------- NOTES ---------------------------------
%
%   Possible improvements for future versions:
%       - allow for a cell array of strings at the input, resulting in a
%         cell array of function handles at the output


% ------------------------ INPUT ARGUMENT CHECKING ------------------------

% Check number of input arguments
if nargin < 1
    error('Not enough input arguments')
elseif nargin > 1
    error('Too many input arguments')    
end

% Check 'string' input argument
% if ~ischar(string)
%     error('Invalid ''string'' input argument. Must be a string.');
% end

% ----------------------------- CALCULATIONS -----------------------------

% Replace string 'Y(degree,order)' with 'solospharm(degree,order,azimuth,elevation,'sch')' (NOTE: Always SN3D!)
% Example: Replace 'Y(3, -4)' with 'solospharm(3,-4,az,elev,'sch')'
% Y             matches     Y
% (\D*)         matches     (
% (\d*)         matches     3                               and replaces $2
% ([^\-\d]+)    matches     ,
% ([\-]?)       matches     -           (if it exists)      and replaces $4
% (\D*)         matches     whitespace  (if it exists)
% (\d*)         matches     4                               and replaces $6
% ([^\)]?)      matches     )           (TODO: Seems to fail)

string = regexprep(string,'Y(\D*)(\d*)([^\-\d]+)([\-]?)(\D*)(\d*)([^\)]?)', 'solospharm($2,$4$6,az,elev,''sch''');
% NOTE: No closing ), since the above match for it fails.
% However, note that the present solution appears to be quite robust for complex strings such as 'abs(3*    Y(7,-5))+1+Y(2,1)'.
% Any replacement attempt should thus be tested against such complex cases.

% Replace string '*' with '.*' and do the same for '/' and '^'
string = regexprep(string,'*', '.*');
string = regexprep(string,'/', './');
string = regexprep(string,'^', '.^');

% Add anonymous function argument list at the beginning
string = ['@(az,elev) ', string];

% Evaluation of this string will provide a handle to our anonymous function
h = eval(string);

return
