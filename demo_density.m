%3LD :: DEMO_DENSITY Polyhedron refinement with varying vertex density.
%   This demo shows how to use a minimal energy algorithm with 'dynamic
%   electron charges' to get spherical electron distributions with varying
%   electron density. The initial layout, a geodesic sphere built from an
%   icosahedron, is refined using a density function, which is defined as a
%   handle to a spherical function and passed to 3LD's MINENERGYCONF.
%   Before the refinement is started, the vertices in the symmetry plane of
%   the geodesic sphere are locked. Try uncommenting the line which
%   releases the vertices again to see how this destroys the symmetry in
%   the refinement process.
%
%   If the demo seems not to work, try to clear variables in your workspace
%   ('clear all'). Note that keyboard input is requested from you in the
%   command line to start each animation.
%
%   See also: MINENERGYCONF (3LD), GEOSPHERE (3LD).


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% --------------------------------- DEMO ---------------------------------

% Build the initial layout
p = geosphere('ico',[2],1);

% Lock the vertices in the x/y plane
lock = zeros(size(p.vertices,1),1);
[az,elev] = cart3sph(p.vertices);
lock(elev == 0) = 1;

% Uncomment this to unlock speakers and try the demo this way
%lock(elev == 0) = 0;

% Define the density function and plot it
density = @(az,elev) abs(sin(elev)) + 0.1;

input('Press return to plot density function.');
plot3LD(density);

input('Press return to plot layout refinement process.');
h = plot3LD(p,lock);view(30,30);

for i=1:30
    p.vertices = minenergyconf(p.vertices,1,density,[],lock);
    set(h,'Vertices',p.vertices);
    drawnow;
end