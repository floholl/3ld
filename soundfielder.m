function [pressure,velocity,velocityVector,uVelocity] = soundfielder(sourcePosition,freq,type,sinkX,sinkY,sinkZ,time,sourceGain,superpone,direction,temperature)

%3LD :: SOUNDFIELDER Sound pressure and velocity fields.
%   [P,V,VV,UV] = SOUNDFIELDER(SRC,FREQ,TYPE,X,Y,Z,TIME[,GAIN,SUM,DIR,T])
%   calculates the complex sound pressure, velocity, velocity vector, and
%   u velocity of soundfields in air, created by S monochromatic sound
%   sources with different radiation characteristics. The positions of the
%   sources and sinks of the field can be arbitrarily defined.
%   
%   SRC is an S-by-3 array containing the x,y,z position information of the
%   S sound sources. The same coordinate system as in Matlab's SPH2CART is
%   applied.
%
%   FREQ is a scalar or a vector with lenght S, either refering to an
%   identical frequency of all monochromatic sound sources, or specifying
%   the frequency of each source independently.
%
%   TYPE is a string which determines the radiation characteristics of the
%   sound sources. Possible choices are 'spherical'/'sph'/'s' or
%   'plane'/'pl'/'p'. If TYPE is a single string, all sound sources will be
%   of that type. If can also be a cell array of S strings, specifying the
%   type for each of the sources independently.
%
%   X, Y, and Z are arrays specifying the x, y, and z coordinates of the
%   sinks, i.e. the measuring points of the soundfield. The same coordinate
%   system as in Matlab's SPH2CART is applied. Use the output of Matlab's
%   MESHGRID for these arrays, i.e. 3D arrays for cubic, 2D arrays for
%   plane, or vectors for line sink definition. SOUNDFIELDER uses the
%   number of dimensions for gradient evaluation of the output vector
%   fields, thus you have to follow the described scheme to get useful
%   output for the three output arguments V, VV, and UV. In all cases,
%   the arrays have to be of identical size, and an equal number of
%   dimensions is used in the output arrays to represent them. The only
%   exception is given when X and Y are vectors and Z is a scalar, in which
%   case SOUNDFIELDER does the meshgriding itself such that all
%   combinations of x and y at constant height z are calculated, i.e. the
%   soundfield on a horizontal plane. Accordingly, two dimensions are used
%   in the output arrays to represent the sinks.
%
%   TIME is a vector of the points in time at which the soundfield is
%   rendered, which have to be specified in seconds.
%
%   GAIN determines the gain factors of the S sound sources. It can either
%   be a scalar, refering to identical gains for all sources at each point
%   of time for which the field is rendered, or a vector of length S,
%   representing time-constant gains for each source, or an S-by-N array,
%   where the element (i,j) represents the gain of the i-th sound source at
%   the j-th point in time. Note that GAIN can be complex, specifying the
%   amplitude as well as the phase of a wavefront.
%
%   SUM is a scalar determining whether the soundfields created by the S
%   sources are superponed in the output arrays P and V, which is the case
%   if SUM is a non-negative scalar.
%
%   DIR specifies the direction of the wavefronts. Non-negative numbers
%   refer to the incoming wavefront, while negative numbers specify an
%   outgoing wavefront. DIR can either be a scalar or a vector of
%   length S, depending on whether all S sound sources share the same
%   direction or not.
%
%   T is the time-invariant and homogeneous temperature at which the
%   soundwaves propagate. It is specified in Kelvin and defaults to 273.15
%   Kelvin (0�C) if not specified.
%
%   P is the complex sound pressure field. The first dimensions of P refer
%   to the dimensions of X,Y,Z (unless those were specified as two vectors
%   and a scalar -> see X,Y,Z), followed by another dimension representing
%   the various points of time for which the field is rendered. If SUM~=0,
%   an additional dimension refers to the fields of the different sound
%   sources. Thus, generally size(P) = [size(X),lenght(TIME),size(SRC,1)].
%   However, any singleton dimension will be removed, e.g. if you calculate
%   the soundfields caused by three sources in a cubic sink grid at a
%   single point of time, size(P) = [size(X),3]. If you additionally
%   superpone the fields created by the sources, size(P) = size(X).
%
%   V is the complex sound velocity field as discussed in [1]. As in P, its
%   first dimensions refer to the dimensions of X, Y, Z. The first
%   dimension after these represents the vector components of the gradient
%   field and thus has as many elements as there are non-singleton
%   dimensions in X, Y, Z, i.e. three if the sink data is cubic, two if it
%   is plane, and one if it represents a line. The next dimension
%   represents the elements in TIME, and if SUM~=0, another dimension
%   refers to the velocity fields due to the different sources of the
%   field. Thus, generally
%   size(V) = [size(X),numNonSingletonSinkDims,length(TIME),size(SRC,1)],
%   but as in P, all singleton dimensions are removed, so if you calculate
%   the superponed field of S sources along a line of 10 points in space
%   for 17 points in time, size(V) = [10 17].
%
%   VV is the complex velocity vector field, as defined in [2]. It is an
%   array  with the same size and properties as V. Its real part is
%   associated with the perception of direction in a soundfield, and its
%   imaginary part is often refered to in the literature as 'phasiness'. As
%   in P, all singleton dimensions will be removed.
%
%   UV is the complex u velocity as defined in [1]. It is an array with the
%   same size and properties as V. Its real part is refered to in the
%   literature as 'active velocity', and is associated with the perception
%   of direction in a soundfield. Its imaginary part is refered to as
%   'reactive velocity' and does not relate to sound energy transport. As
%   in P, all singleton dimensions will be removed.
%
%   See also: PRESSURE_ERRORS (3LD), DIRECTION_DEVIATION (3LD), GRADIENT,
%             MESHGRID.
%
%   Reference:
%     [1] M. A. Poletti, "A Unified Theory of Horizontal Holographic Sound
%         Systems", Journal of the Audio Engineering Society, Vol.48,
%         No.12, December 2000, pp.1155-1182.
%     [2] J. Daniel, "Acoustic Properties and Perceptive Implications of
%         Stereophonic Phenomena", Corrected version, 03/29/99, AES 16th
%         International Conference on Spatial Sound Reproduction, 1999.


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger (floholl_AT_sbox.tugraz.at) and
% (c) 2003 Alois Sontacchi.
% This function bases on Matlab code originally written by Alois
% Sontacchi at the Institute of Electronic Music and Acoustics (IEM) at
% Graz, Austria.

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% --------------------------------- NOTES ---------------------------------
%
% Possible improvements for future versions:
%   - There is a known bug in the calculation of the velocity vector which
%     is activated for soundfields with only a single source. See line 447
%     for more information.
%   - The three vector fields returned by the function (velocity,
%     velocityVector, uVelocity) would need to be further tested regarding
%     their accuracy. Sometimes, the opposite direction of the one that is
%     expected is obtained for the vectors. This can be avoided by smarter
%     derivation of the field's phase, but no globally valid solution has
%     been found so far. See lines 399, 414, and 330 for the relevant
%     information.
%   - There is a slight inefficiency in the function, since two arrays
%     representing the distances from each source to each sink and the
%     direction of each source relative to each sink are always calculated
%     both, although the first is only required for spherical and the
%     second only for plane wavefronts. It makes the code more readable
%     right now, but a smarter solution should be found for this. The
%     according piece of code starts in line 346.
%   - Additional source types, like dipole sources (see Nicol and Emerit,
%     "3D-Sound Reproduction over an Extensive Listening Area: A Hybrid
%     Method Derived From Holophony and Ambisonic", AES 16th International
%     Conference, p.437) should be implemented.
%   - Other gases than air should be allowed for.



% ----- INPUT ARGUMENT CHECKING AND PROCESSING, CONSTANT DEFINITIONS -----

% Check number of input arguments
if nargin < 7
    error('Not enough input arguments.');
elseif nargin > 11
    error('Too many input arguments.');
end


% Check 'sourcePosition' input argument
if size(sourcePosition,2) ~= 3
    error('Invalid ''sourcePosition'' input argument. Must be numeric K-by-3 array.');
end

% The size of the 'sourcePosition' array determines the number of sources
numSources = size(sourcePosition,1);

% Get x, y, and z coordinates of the sources
sourceX = sourcePosition(:,1);
sourceY = sourcePosition(:,2);
sourceZ = sourcePosition(:,3);


% Check 'freq' input argument
if ~isnumeric(freq) || ~isscalar(freq) && numel(freq) ~= numSources
    error('Invalid ''freq'' input argument. Must be numeric scalar or array with numSources elements.');
elseif isscalar(freq)
    % Apply this frequency to all sound sources
    freq = repmat(freq,numSources,1);
else
    % Convert to a vector for internal calculations
    freq = freq(:);
end


% Check 'type' input argument
type = lower(type);  % Safety
if ischar(type)      % Convert to cell array if 'type' is a character array
    type = cellstr(type);
end

if numel(type) ~= numSources && ~isscalar(type)
    % Either define one type for all sources, or one type for each source
    error('Invalid ''type'' input argument. Must be a single string or cell array of strings with numSources elements.');
end

% Recognize type
for i = 1:numel(type)
    if strcmp(type{i},'spherical') || strcmp(type{i},'sph') || strcmp(type{i},'s')
        type{i} = 'spherical';
    elseif strcmp(type{i},'plane') || strcmp(type{i},'pl') || strcmp(type{i},'p')
        type{i} = 'plane';
    else
        error('Invalid entry in ''type'' input argument. Must be ''spherical''/''sph''/''s'' or ''plane''/''pl''/''p''.');
    end
end

% Convert to a vector for internal calculations
type = type(:);


% Check 'sinkX', 'sinkY', and 'sinkZ' input arguments
if ~isnumeric(sinkX) || ~isnumeric(sinkY) || ~isnumeric(sinkZ)
    error('Invalid input argument. ''sinkX'', ''sinkY'', and ''sinkZ'' arrays must be numerical.');
end

if ~isequal(size(sinkX),size(sinkY),size(sinkZ))
    % If sinkX, sinkY are vectors and sinkZ is a scalar, build an x/y plane
    % at constant height sinkZ
    if isvector(sinkX) && isvector(sinkY) && isscalar(sinkZ)
        [sinkX,sinkY] = meshgrid(sinkX,sinkY);
        sinkZ = repmat(sinkZ,size(sinkX));    % Could also be 'size(sinkY)'
    else
        error('Dimensions of sinkX, sinkY, sinkZ input arrays must be identical.');
    end
end

% The size of the 'sinkX' array determines the number of sinks
numSinks = numel(sinkX); % (could also be 'numel(sinkY)' or 'numel(sinkZ)')

% Save original size and number of dimensions for their later restoration
numSinkDims = ndims(sinkX);
sizeSinks = size(sinkX);
tempSize = sizeSinks;
tempSize(tempSize==1) = [];
numNonSingletonSinkDims = length(tempSize);


% Convert to vectors for internal calculations
sinkX = sinkX(:);
sinkY = sinkY(:);
sinkZ = sinkZ(:);


% Check 'time' input argument
if  ~exist('time','var') || isempty(time)
    % Default to a single moment at t=0
    time = 0;
elseif ~isnumeric(time) || ~isvector(time)
    error('Invalid ''time'' input argument. Must be numeric vector.');
end
% Convert to column vector for internal calculations
time = time(:);
% The length of the 'time' vector determines the number of moments
numMoments = length(time);
% Replicate to internal format numSources-by-numSinks-by-numMoments
time = repmat(time,[1,numSources,numSinks]);
time = permute(time,[2 3 1]);


% Check 'sourceGain' input argument
if  ~exist('sourceGain','var') || isempty(sourceGain)
    % Default to a time-invariant gain factor of 1 for all sources
    sourceGain = 1;
elseif isnumeric(sourceGain) && isscalar(sourceGain)
    % Replicate to internal format numSources-by-numSinks-by-numMoments
    sourceGain = repmat(sourceGain,[numSources,numSinks,numMoments]);
elseif isnumeric(sourceGain) && isvector(sourceGain) && length(sourceGain) == numSources
    % Convert to vector for internal calculations
    sourceGain = sourceGain(:);
    % Replicate to internal format numSources-by-numSinks-by-numMoments
    sourceGain = repmat(sourceGain,[1,numSinks,numMoments]);
elseif isnumeric(sourceGain) && isequal(size(sourceGain),[numSources numMoments])
    % That's exactly what we need
else
    error('Invalid ''sourceGain'' input argument. Must be numeric scalar, or vector with numSources elements, or numSources-by numMoments array.');
end


% Check 'superpone' input argument
if  ~exist('superpone','var') || isempty(superpone)
    % Default to 1 if not specified
    superpone = 1;
else
    % Any non-zero value will be interpreted as 1
    superpone = superpone ~= 0;
end


% Check 'direction' input argument
if  ~exist('direction','var') || isempty(direction)
    % Default to -1 /outgoing wavefront) if not specified
    direction = -1;
elseif ~isnumeric(direction) || ~isscalar(direction) && numel(direction) ~= numSources
    error('Invalid ''direction'' input argument. Must be scalar or or array with numSources elements.');
end
direction = sign(direction) + (direction == 0); % Clip to +/-1. Zeros are considered positive.
if isscalar(direction)
    % Replicate to internal format numSources-by-numSinks-by-numMoments
    direction = repmat(direction,[numSources,numSinks,numMoments]);
else
    % Replicate to internal format numSources-by-numSinks-by-numMoments
    direction = repmat(direction(:),[1,numSinks,numMoments]);
end


% Check 'temperature' input argument
if  ~exist('temperature','var') || isempty(temperature)
    % Default to 0�C = 273.15�K if not specified
    temperature = 273.15;
elseif ~isnumeric(temperature) || ~isscalar(temperature) || temperature < 0
    error('Invalid ''temperature'' input argument. Must be non-negative scalar.');
end

% Adiabatic index of air
adiabaticIndex = 1.402;

% Universal gas constant in [J/(K*mol)]
universalGasConstant = 8.31447;

% Molecular weight of air in [kg/mol]
molarMass = 0.0289644;

% Calculate speed of sound as a function of medium and temperature
speedOfSound = sqrt(adiabaticIndex * universalGasConstant * temperature / molarMass);

% Wave number
waveNumber = 2 * pi * freq / speedOfSound;  % Scalar
% Replicate to internal format numSources-by-numSinks-by-numMoments
waveNumber = repmat(waveNumber,[1,numSinks,numMoments]);

% Standard atmospheric pressure at sea level in [Pa]
pressure = 101325;

% Calculate gas density as a function of pressure, medium, and temperature
density = (pressure * molarMass) / (universalGasConstant * temperature); % see Physikbuch, Eq.(W19)

% Do some meshgriding to get arrays of size numSources-by-numSinks
[sinkX,sourceX] = meshgrid(sinkX,sourceX);
[sinkY,sourceY] = meshgrid(sinkY,sourceY);
[sinkZ,sourceZ] = meshgrid(sinkZ,sourceZ);


% ----------------------------- CALCULATIONS -----------------------------

% We'll need the distance between each source and sink for spherical waves
distSrcSink = sqrt( (sinkX-sourceX).^2 + (sinkY-sourceY).^2 + (sinkZ-sourceZ).^2 );  % This is numSources-by-numSinks
% Replicate to internal format numSources-by-numSinks-by-numMoments
distSrcSink = repmat(distSrcSink,[1,1,numMoments]);

% We'll need the direction of each source to each sink for plane waves
dirSrc = sourceX.*sinkX + sourceY.*sinkY + sourceZ.*sinkZ;  % This is numSources-by-numSinks
% Replicate to internal format numSources-by-numSinks-by-numMoments
dirSrc = repmat(dirSrc,[1,1,numMoments]);

% TODO: FOR UNIFORM SOURCE TYPES, IT'S INEFFICIENT TO ALWAYS CALCULATE
% BOTH, DIRECTION AND DISTANCE, SINCE THE DISTANCE IS IS ONLY REQUIRED FOR
% SPHERICAL, AND THE DIRECTION ONLY FOR PLANE SOURCES. IT MAKES THE CODE
% EASIER TO READ THOUGH THAN USING A SINLGE VARIABLE FOR BOTH. TO BE
% IMPROVED IN A FUTURE VERSION.

% Case distinction for reasons of efficiency
if isscalar(type)
    % Call subfunction once for identical source types
    pressure = getPressureField(type{1},time,direction,sourceGain,distSrcSink,dirSrc,waveNumber,speedOfSound);   
    
else
    % Call subfunction in for-loop for mixed source types
    pressure = [];  % Initialization for later use of CAT 
    for i = 1:numSources
        size(type),size(time),size(direction),size(sourceGain),size(distSrcSink),size(dirSrc),size(waveNumber)
        % Get field created by this source
        thisPressure = getPressureField(type{i},time(i,:,:),direction(i,:,:),sourceGain(i,:,:),distSrcSink(i,:,:),dirSrc(i,:,:),waveNumber(i,:,:),speedOfSound);
        % Add this field to the array with the fields of all sources
        pressure = cat(1,pressure,thisPressure);
    end
    
end

% Permute to format size(pressure) = [sizeSinks,numMoments,numSources]
pressure = permute(pressure,[2 3 1]);
pressure = reshape(pressure,[sizeSinks numMoments numSources]);

% Initialization (required!)
velocity = [];
velocityVector = [];
uVelocity = [];

if nargout > 1
    
    % Permute to sizeSinks-by-numMoments-by-numSources
    waveNumber = permute(waveNumber,[2 3 1]);
    waveNumber = reshape(waveNumber,[sizeSinks numMoments numSources]);
    
    % This is the case of sinks along a line
    if numNonSingletonSinkDims == 1
        % This nested loop cannot be avoided due to the use of GRADIENT!
        for i = 1:numMoments
            for j = 1:numSources
                tempPressure = pressure(:,i,j);     % Temporary variables
                tempWaveNum = waveNumber(:,i,j);
                pressureGradientX = gradient( abs(tempPressure) );
                % The CONJ is required to get the direction of the gradient right
                phaseGradientX = gradient( unwrap(angle(conj(tempPressure))) );
                % Calculate complex velocity field (eq. 89 in [1])
                velocity(:,1,i,j) = tempPressure .* (phaseGradientX + j * pressureGradientX ./ abs(tempPressure)) ./ tempWaveNum * density * speedOfSound;
            end
        end
    
    % This is the case of the sinks defining a plane
    elseif numNonSingletonSinkDims == 2
        % This nested loop cannot be avoided due to the use of GRADIENT!
        for i = 1:numMoments
            for j = 1:numSources
                tempPressure = pressure(:,:,i,j);     % Temporary variables
                tempWaveNum = waveNumber(:,:,i,j);
                [pressureGradientX,pressureGradientY] = gradient( abs(tempPressure) );
                % The CONJ is required to get the direction of the gradient right
                [phaseGradientX,phaseGradientY] = gradient( unwrap(angle(conj(tempPressure))) );
                % Calculate complex velocity field (eq. 89 in [1])
                velocity(:,:,1,i,j) = tempPressure .* (phaseGradientX + j * pressureGradientX ./ abs(tempPressure)) ./ tempWaveNum * density * speedOfSound;
                velocity(:,:,2,i,j) = tempPressure .* (phaseGradientY + j * pressureGradientY ./ abs(tempPressure)) ./ tempWaveNum * density * speedOfSound;
            end
        end
    
    % This is the case of a cubic distribution of sinks
    elseif numNonSingletonSinkDims == 3
        % This nested loop cannot be avoided due to the use of GRADIENT!
        for i = 1:numMoments
            for j = 1:numSources
                tempPressure = pressure(:,:,:,i,j);     % Temporary variables
                tempWaveNum = waveNumber(:,:,:,i,j);
                [pressureGradientX,pressureGradientY,pressureGradientZ] = gradient( abs(tempPressure) );
                % The CONJ is required to get the direction of the gradient right
                [phaseGradientX,phaseGradientY,phaseGradientZ] = gradient( unwrap(angle(conj(tempPressure))) );
                % Calculate complex velocity field (eq. 89 in [1])
                velocity(:,:,:,1,i,j) = tempPressure .* (phaseGradientX + j * pressureGradientX ./ abs(tempPressure)) ./ tempWaveNum * density * speedOfSound;
                velocity(:,:,:,2,i,j) = tempPressure .* (phaseGradientY + j * pressureGradientY ./ abs(tempPressure)) ./ tempWaveNum * density * speedOfSound;
                velocity(:,:,:,3,i,j) = tempPressure .* (phaseGradientZ + j * pressureGradientZ ./ abs(tempPressure)) ./ tempWaveNum * density * speedOfSound;
            end
        end
    
    end
    
    % now: size(velocity) = [sizeSinks,numNonSingletonSinkDimensions,numMoments,numSources]
    
end

if nargout > 2
    % Calculate complex velocity vector (eq.87 in [1])
    tempPressure = repmat(pressure,[ones(1,length(size(pressure))),numNonSingletonSinkDims]);
    % TODO: THE NEXT LINE IS BUGGY FOR SINGLE SOUND SOURCES, FOR WHICH THE LAST
    % DIMENSION OF pressure IS SINGLETON AND THUS SQUEEZED. THIS RESULTS IN
    % REPEATED INDICES FOR THE permute IN THE FOLLOWING LINE!
    tempPressure = permute(tempPressure,[1:numSinkDims,ndims(tempPressure),ndims(tempPressure)-2,ndims(tempPressure)-1]);
    velocityVector = density * speedOfSound * velocity ./ tempPressure;
    % now: size(velocityVector) = size(velocity)
end

if nargout > 3
    % Calculate complex u velocity (eq.108 in [1])
    normVelocityVector = repmat(sqrt(sum(velocityVector.^2,numSinkDims+1)),[ones(1,numNonSingletonSinkDims),numNonSingletonSinkDims,1,1]);
    uVelocity = 2 * speedOfSound * conj(velocityVector) ./ (1 + normVelocityVector.^2);
    % now: size(uVelocity) = size(velocity)
end

if superpone  
    % Superpone the fields created by the single sources.
    pressure       = sum(pressure,numSinkDims+2);
    velocity       = sum(velocity,numSinkDims+3);
    velocityVector = sum(velocityVector,numSinkDims+3);
    uVelocity      = sum(uVelocity,numSinkDims+3);
end

% Remove singleton dimensions
pressure       = squeeze(pressure);
velocity       = squeeze(velocity);
velocityVector = squeeze(velocityVector);
uVelocity      = squeeze(uVelocity);

return



% ------------------------------ SUBFUNCTION ------------------------------


    function pressure = getPressureField(type,time,direction,sourceGain,distSrcSink,dirSrc,waveNumber,speedOfSound)
    
    switch type
        
        case 'plane'
            % Calculate complex sound pressure of a plane wavefront
            pressure = sourceGain .* exp( j * waveNumber .* (speedOfSound * time + direction .* dirSrc) );
        
        case 'spherical'
            % Calculate complex sound pressure of a spherical wavefront
            pressure = (sourceGain ./ distSrcSink) .* exp( j * waveNumber .* (speedOfSound * time + direction .* distSrcSink) );
            
    end
    
    return