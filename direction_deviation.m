function directionDeviation = direction_deviation(referenceDirection,synthesizedDirection,dim)

%3LD :: DIRECTION_DEVIATION soundfield direction deviation.
%   DIRDEV = DIRECTION_DEVIATION(REFDIR,SYNTHDIR) computes errors among the
%   directions of two vector fields. REFDIR and SYNTHDIR are two arrays of
%   equal size, representing the vector fields which indicate the direction
%   of a complex reference pressure field and a synthesized field.
%   Direction indicators are for example the real part of the complex
%   velocity or the real part of the u velocity as calculated by 3LD's
%   SOUNDFIELDER.
%
%   DIM specifies the dimension which represents the x,y,and possibly z
%   components of the vector field. This dimension will be missing in the
%   output array DIRECTION_DEVIATION: since the direction deviation is
%   specified as a scalar in radians at each point of the field, the
%   dimensions representing the vector field components becomes singleton
%   and is removed by the function.
%
%   See also: SOUNDFIELDER (3LD), PRESSURE_ERRORS (3LD).
%
%
%   References:
%       [1] Florian Hollerweger: Periphonic Sound Spatialization in Multi-User
%       Virtual Environments. Master thesis, Institute of Electronic Music and
%       Acoustics, University of Music and Dramatic Arts, Graz, Austria, 2006.


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger (floholl_AT_sbox.tugraz.at),
% and (c) 2003 Alois Sontacchi.
% This function bases on Matlab code originally written by Alois
% Sontacchi at the Institute of Electronic Music and Acoustics (IEM) at
% Graz, Austria.

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ------------------------ INPUT ARGUMENT CHECKING ------------------------

% Check number of input arguments
if nargin < 3
    error('Not enough input arguments')
elseif nargin > 3
    error('Too many input arguments')    
end

% Check dimensions of input arrays 'direction1', 'direction2'
if ~isequal(size(referenceDirection),size(synthesizedDirection))
    error('Invalid input arrays. Must both be of size N-by-2 or N-by-3.');
end

% Check 'dim' input argument
if ~isnumeric(dim) || ~isscalar(dim)
    error('Invalid ''dim'' input argument. Must be numeric scalar.');
end


% ----------------------------- CALCULATIONS -----------------------------

% 3D generalized version of direction deviation (ch. 17.3 in [2])
directionDeviation = acos( dot(referenceDirection,synthesizedDirection,dim) ./ ( sqrt(sum(referenceDirection.^2,dim)) .* sqrt(sum(synthesizedDirection.^2,dim) ) ) );

% Remove singleton dimension
directionDeviation = squeeze(directionDeviation);

return