%3LD :: DEMO_SOUNDFIELDER Plot wavefront propagation of a Hanning-weighted impulse.
%   This demo shows the wavefront propagation in the x/y plane of a
%   Hanning-weighted impulse, which is a superposition of several
%   monochromatic sound sources with linearly increasing frequencies (e.g.
%   100Hz, 200Hz, 300Hz, 400Hz, ...) and gain factors decaying with the
%   right half of a hanning window. The original field is then reproduced
%   by the means of Vector Base Panning on a tetrahedron loudspeaker layout
%   and the according wavefront reconstruction is plotted as well. The
%   final animation shows the squared sound pressure error of the
%   reconstructed field.
%
%   If the demo seems not to work, try to clear variables in your workspace
%   ('clear all'). Note that keyboard input is requested from you in the
%   command line to start each animation.
%
%   See also: SOUNDFIELDER (3LD), PLATONICSOLID (3LD), VBP (3LD), HANNING.


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ---------------------------- USER PARAMETERS ----------------------------

% Number of pics in the animation
num_pics = 30;

% Specify sound source position and radiation pattern
position = [2 0 0];     % x,y,z
type={'spherical'};     % 'spherical', 'plane'

% Define center frequency and upper freq. limit of impulse partials
center_freq = 100;
upper_freq = 500;


% -------------------- CREATE HANNING-WEIGHTED IMPULSE --------------------

freq = [center_freq:center_freq:upper_freq];
num_sources = length(freq);

% Define gains of Hanning-weighted impulse partials
window = hanning(2 * length(center_freq:center_freq:upper_freq));
gain = window( ceil(length(window)/2)+1 : length(window) );

% Use same source position for all impulse partials
position = repmat(position,num_sources,1);

% Specify sink positions (here in the x/y plane)
x = -1:0.05:1;
y = -1:0.05:1;
z = 0;

% Define time points for rendering
time = 0 : 1/(center_freq*num_pics) : 1/center_freq - 1/(center_freq*num_pics);


% -------------------------- ORIGINAL SOUNDFIELD --------------------------

% Render the sound pressure field
pressure = soundfielder(position,freq,type,x,y,z,time,gain);

input('Press return to plot the original wavefront.');
for i = 1:num_pics
    surf(x,y,real(pressure(:,:,i))),
    title('Original soundfield'), shading interp, colormap gray,
    view(0,90), grid off, axis([-1 1 -1 1 -num_sources num_sources -1 1]), axis square
    animation = getframe;
end


% -------------------------- LOUDSPEAKER LAYOUT --------------------------

% Define loudspeaker layout properties
spk = platonicsolid('tetra',2);
[spk_az, spk_elev] = cart3sph(spk.vertices);
spk_type = 'spherical';

% Get sound source direction
[src_az, src_elev] = cart3sph(position);

% Derive VBP loudspeaker gains. You can also try AMB3D_ENCODER and
% AMB3D_DECODER instead.
spk_gain = vbp([src_az, src_elev],[spk_az, spk_elev],spk.faces,'vbap',gain);


% ----------------------- RECONSTRUCTED SOUNDFIELD -----------------------

% Each loudspeaker contributes to the reconstruction of each partial, so we
% accordingly replicate our frequency matrix
num_speakers = size(spk_gain,1);
freq_rep = repmat(freq,num_speakers,1);
freq_rep = freq_rep(:);

% Superpone the num_speakers*num_sources soundfields of each loudspeaker
% recreating each partial
pressure_vbp = soundfielder(repmat(spk.vertices,num_sources,1),...
                            freq_rep,spk_type,x,y,z,time,spk_gain(:),1);
                        
pause(1);
input('Press return to plot the VBP reconstructed wavefront.');
for k = 1:num_pics
    surf(x,y,real(pressure_vbp(:,:,k))),
    title('VBP reconstructed soundfield'), shading interp, colormap gray,
    view(0,90), grid off, axis([-1 1 -1 1 -num_sources num_sources -1 1]), axis square
    animation = getframe;
end


% ------------------------- RECONSTRUCTION ERROR -------------------------

pause(1.5);
input('Press return to plot the squared sound pressure error.');
sq_error = pressure_errors(pressure,pressure_vbp);
for k = 1:num_pics
    surf(x,y,real(sq_error(:,:,k))),
    title('Squared sound pressure error'), shading interp, colormap gray,
    view(0,90), grid off, axis([-1 1 -1 1 -num_sources^2 num_sources^2 -1 1]), axis square
    animation = getframe;
end