function vertices = minenergyconf(initialLayout,numIterations,densityFct,radius,lockStatus,powRepulsion)

%3LD :: MINENERGYCONF Minimal energy electron configurations.
%   V = MINENERGYCONF(E[,N,RADIUS,REPULSION,DENSITY,LOCK]) simulates the
%   process of electrons distributing themselves over the surface of a
%   sphere until they reach what is refered to as a 'minimal energy
%   configuration', i.e. a natural equilibrum of minimal potential energy.
%   In MINENERGYCONF, this algorithm has been extended to allow for
%   arbitrary surface shapes, a spherical electron density function,
%   scalable repulsion forces, and 'locked' electrons.
%
%   E can be a scalar, defining the number of electrons in a new
%   configuration. In this case, the initial positions of the E electrons
%   will be randomly distributed. E can also be an F-by-3 array,
%   representing the x,y,z coordinates of an existing configuration with F
%   electrons, which is then used as the initial layout for further
%   modification. The same coordinate system as in Matlab's CART2SPH is
%   applied.
%
%   N represents the number of iterations applied. In each iteration, the
%   repulsion forces among all possible electron pairs are calculated, and
%   the electrons are moved to their according new positions. If not
%   specified, N defaults to 1.
%
%   RADIUS determines the radius of the configuration. If RADIUS is a
%   scalar, the electrons will be distributed on a sphere with that radius.
%   However, RADIUS can also represent a handle to a spherical function, in
%   which case it represents a surface with a radius depending on azimuth
%   (first argument) and elevation (second argument). Both angels should be
%   specified in radians and follow a coordinate system as used in Matlab's
%   CART2SPH. If not specified, RADIUS defaults to 1.
%
%   REPULSION specifies the power of the repulsion forces among the
%   electrons. If not specified, REPULSION defaults to 2, which represents
%   the natural case of repulsion forces between two electrons, which are
%   proportional to their inverse square distance. If e.g. REPULSION=1, the
%   forces will be proportional to their plain inverse distance.
%
%   DENSITY is a handle to a spherical function, representing the electron
%   density as a function of direction (azimuth = first argument,
%   elevation = second argument), in exactly the same way as RADIUS defines
%   the radius as a spherical function. Higher repulsion forces among the
%   electrons will occur in areas of lower density and vice versa.
%   The repulsion forces among two electrons are then proportional to the
%   product of the inverse densities at their positions. If not specified,
%   DENSITY defaults to a constant density of 1 over the entire surface.
%
%   LOCK is a vector with a length matching the number of electrons in the
%   configuration. A non-zero entry means that the electron with the
%   according row index in the output array V will be 'locked', i.e. it
%   will exercise repulsion forces on the other electrons, but is immune to
%   the forces exercised on itself and will thus remain in its initial
%   position. If not specified, all electrons remain unlocked, i.e. LOCK is
%   a null vector. If LOCK is undersized, the remaining electrons will be
%   considered unlocked.
%
%   V is an E-by-3 matrix specifying the resulting x,y,z coordinates of the
%   E electrons.
%
%   See also: PLATONICSOLID (3LD), GEOSPHERE (3LD).


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger (floholl_AT_sbox.tugraz.at) and
% (c) 2005 Will Wolcott.
% This function bases on C code originally written by Will Wolcott at the
% Graduate Program in Media Arts and Technology at the University of
% California Santa Barbara.
%
% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% --------------------------------- NOTES ---------------------------------
%
%   Possible improvements for future versions:
%       - Avoid nested for loops and implement more efficiently.


% ----- INPUT ARGUMENT CHECKING AND PROCESSING, CONSTANT DEFINITIONS -----

% Check number of input arguments
if nargin < 1
    error('Not enough input arguments.')
elseif nargin > 6
    error('Too many input arguments.')    
end


% Check 'radius' input argument
if  ~exist('radius','var') || isempty(radius)
    % Default to constant radius of 1 if not specified
    radiusFct = @(az,elev) 1;
elseif isnumeric(radius) && isscalar(radius)
    % If radius is a scalar, assume spherical surface
    radiusFct = @(az,elev) radius;
elseif isa(radius,'function_handle')
    % If radius already is a function handle, simply use that one
    radiusFct = radius;
else
    error('Unknown ''radius'' input argument. Must be a scalar numeric value or function handle.');
end


% Check 'initialLayout' input argument
if isnumeric(initialLayout) && isscalar(initialLayout)
    % If initialLayout is a scalar, interpret it as the number of
    % electrons in a new configuration
    numElectrons = initialLayout;
    % Initialize electron position matrix with random directions
    az = rand(numElectrons,1) * 2*pi;
    elev = (rand(numElectrons,1) * 2*pi) - pi;
    r = radiusFct(az,elev);
    r = r(:);   % Required safety
    
    % Convert to cartesian coordinates for internal processing
    [x,y,z] = sph2cart(az,elev,r);
    vertices(:,1) = x;
    vertices(:,2) = y;
    vertices(:,3) = z;
    
elseif size(initialLayout,2) == 3
    % If size(initialLayout) = [N,3], interpret as an existing configuration
    % and use its number of electrons and coordinates for initialization
    vertices = initialLayout;
    numElectrons = size(vertices,1);
    
else 
    error('Unknown ''initialLayout'' input argument. Must be a scalar or an N-by-3 array.')

end


% Check 'numIterations' input argument
if  ~exist('numIterations','var') || isempty(numIterations)
    % Default to a single iteration if not specified
    numIterations = 1;
elseif ~isscalar(numIterations)
    error('Unknown ''numIterations'' input argument. Must be a scalar.');
end


% Check 'densityFct' input argument
if ~exist('densityFct','var') || isempty(densityFct)
    % Default to constant electron density if not specified
    densityFct = @(az,elev) 1;
elseif ~isa(densityFct,'function_handle')
    error('Unknown ''densityFct'' input argument. Must be a function handle.');
end


% Check 'lockStatus' input argument
if  ~exist('lockStatus','var') || isempty(lockStatus)
    % Default to only unlocked electrons if not specified
    lockStatus = zeros(numElectrons,1);
elseif numel(lockStatus) > numElectrons
    % Ignore additional values
    lockStatus = lockStatus(1:numElectrons);
    warning('Clipped oversized ''lockStatus'' input array to required amount of entries.')
elseif numel(lockStatus) < numElectrons
    % Set missing electrons to 'unlocked'
    lockStatus(end+1:numElectrons) = 0;
    warning('Set missing entries in ''lockStatus'' input array to zero.');
end
% Make this a column vector for internal calculations
lockStatus = lockStatus(:);


% Check 'powRepulsion' input argument
if  ~exist('powRepulsion','var') || isempty(powRepulsion)
    % Default to repulsion proportional to inverse square distance if not
    % specified
    powRepulsion = 2;
elseif ~isscalar(powRepulsion)
    error('Unknown ''powRepulsion'' input argument. Must be a scalar.');
end



% ----------------------------- CALCULATIONS -----------------------------

% Constant scaling factor for the repulsion forces (arbitrary choice)
scalingFactor = (.1/numElectrons);

% Only make an effort if there is actually something to do
if numIterations > 0
    
    % For the specified number of iterations...
    % TODO: TRY TO VECTORIZE NESTED FOR-LOOPS IN FUTURE VERSIONS
    for k = 1:numIterations
        
        % Verbose for better prediction of coffee break length
        %disp(['minenergyconf working out iteration # ' num2str(k) ' ...']);
        
        % ... calculate repulsion forces exercised by electron i...
        for i = 1:numElectrons
            
            % ... onto electron j
            for j = 1:numElectrons
                
                % Identical electrons? Locked electron? If so, skip it
                if j ~= i && lockStatus(j) == 0
                    
                    % Distance D between electrons j and i
                    distVect = vertices(j,:) - vertices(i,:);
                    distance = sqrt(sum(distVect.^2));
                    
                    % Safety: avoid division by zero
                    if distance < 0.01
                        distance = 0.01;
                    end
                    
                    % Get directions of electrons j, i to derive their charges
                    [jAz,jElev] = cart2sph(vertices(j,1),vertices(j,2),vertices(j,3));
                    [iAz,iElev] = cart2sph(vertices(i,1),vertices(i,2),vertices(i,3));
                    
                    % Product Qj * Qi of the electron charges: the charge
                    % of an electron in a given direction is inversely
                    % proportional to the electron density there
                    chargeProduct = 1 / ( densityFct(jAz,jElev) * densityFct(iAz,iElev) );

                    % Repulsion factor (Qj*Qi)/D^p for this electron pair
                    repulsionFactor = chargeProduct / (distance.^powRepulsion);

                    % Move electron j to its new position
                    vertices(j,:) = vertices(j,:) + scalingFactor * repulsionFactor * distVect;
                    
                    % Bring electron j back to its specified surface
                    vertices(j,:) = map_to_radius(vertices(j,:),radiusFct);
                    
                end
                
            end
            
        end
        
    end
    
else
    
    % If no iterations are to be done, only map radius to specified surface
    vertices = map_to_radius(vertices,radiusFct);
    
end

return



% -------------------------------SUBFUNCTION-------------------------------


    function vertices = map_to_radius(vertices,radiusFct)
    
    % Get direction
    [az,elev] = cart2sph( vertices(:,1),vertices(:,2),vertices(:,3) );
    r = radiusFct(az,elev); % Calculate radius for that direction
    r = r(:);               % Convert to column vector
    [vertices(:,1), vertices(:,2), vertices(:,3)] = sph2cart(az,elev,r);
    
    return