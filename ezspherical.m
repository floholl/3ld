function h = ezspherical(radiusFct,gridN)

%3LD :: EZSPHERICAL Plot spherical functions.
%   H = EZSPHERICAL(FCT[,NGRID]) is an easy-to-use plotter for spherical
%   functions. FCT must be a function handle with two arguments, the first
%   of which is interpreted as the azimuth, and the second one of which is
%   interpreted as the elevation. The same spherical coordinate system as
%   in Matlab's SPH2CART is applied. NGRID specifies the resolution at
%   which the plot is calculated: for both angles, a function value is
%   calculated at every interval 2*pi/NGRID. EZSPHERICAL actually plots the
%   absolute value of the radius in each direction. Positive and negative
%   function values can be distinguished by color (red for +1, blue for -1
%   in the default case). Note that EZSPHERICAL uses Matlab's SURF, so you
%   can set the properties of the returned graphic handle H as in SURF.
%
%   Example:
%   EZSPHERICAL can be used to plot spherical harmonic functions, created
%   by SPHARMONIC (3LD library). Use HANDLESPHARM (3LD library) to create
%   an according function handle. For example, to plot the spherical
%   harmonic function of degree 1, order 1, and superscript -1:
%       fct = handlespharm('Y(1,-1)');
%       h = ezspherical(fct);
%
%   See also: HANDLESPHARM (3LD), SOLOSPHARM (3LD), SPHARMONIC (3LD),
%             SURF, EZPOLAR.


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ----- INPUT ARGUMENT CHECKING AND PROCESSING, CONSTANT DEFINITIONS -----

% Check number of input arguments
if nargin < 1
    error('Not enough input arguments.')
elseif nargin > 2
    error('Too many input arguments.')    
end

% Check 'radiusFct' input argument
if ~isa(radiusFct,'function_handle');
    error('Invalid ''radiusFct'' input argument. Must be a function handle.');
end

% Check 'gridN' input argument
if ~exist('gridN','var') || isempty(gridN)
    gridN = 60;
elseif ~isnumeric(gridN) || ~isscalar(gridN)
    error('Invalid ''gridN'' input argument. Must be numeric scalar.');
end


% ----------------------------- CALCULATIONS -----------------------------

% Create grid
delta = 2*pi/gridN;
azimuth = 0 : delta : 2*pi;
elevation = -pi/2 : delta : pi/2;
[azimuth,elevation] = meshgrid(azimuth,elevation);

% Calculate radius at the points specified by the grid
radius = radiusFct(azimuth,elevation);

% How do _you_ plot negative radii? :-) You will be able to distinguish
% positive from negative values through the plot's color map.
absRadius = abs(radius);

% Convert to cartesian coordinates for plotting with 'surf'
[x,y,z] = sph2cart(azimuth,elevation,absRadius);

% Plot the surface
h = surf(x,y,z,radius);

return