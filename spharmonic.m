function Y = spharmonic(degree,azimuth,elevation,normalize)

%3LD :: SPHARMONIC spherical harmonic functions.
%   Y = SPHARMONIC(N,AZ,ELEV[,NORM]) computes the spherical harmonic
%   functions of degree N and order M = 0, 1, ..., N for both superscripts
%   SIG = +-1. The functions are evaluated for each element of AZ and ELEV.
%   N must be a scalar integer and AZ and ELEV must be arrays of identical
%   size containing the azimuth and elevation arguments in radians. NORM is
%   an optional argument, specifying different normalizations of the
%   Legendre polynomials, which are included in the spherical harmonics.
%   Legal terms are 'unnorm','sch' or 'norm', and default is 'unnorm'.
%   SPHARMONIC calls Matlab's LEGENDRE with this argument; more information
%   on the normalization options can be found there.
% 
%   Y is an (2*N+1)-by-L matrix, where L = length(AZ) = lenght(ELEV).
%   The Y(M*SIG+N+1,i) entry corresponds to the spherical harmonic function 
%   of degree N, order M, and superscript SIG, evaluated at AZ(i) and
%   ELEV(i).
% 
%   In general, the returned array has one more dimension than AZ and ELEV.
%   Each element Y(M*SIG+N+1,i,j,k,...) contains the spherical harmonic
%   function of degree N, order M, and superscript SIG, evaluated at
%   AZ(i,j,k,...) and ELEV(i,j,k,...).
% 
%   For a spherical coordinate system as used in Matlab (see CART2SPH), the
%   spherical harmonic functions are:
%                                                 _
%                                                |  sin(M*AZ)   for SIG=-1
%       Y(N,M,SIG;AZ,ELEV) = P(N,M;sin(ELEV)) * <
%                                                |_ cos(M*AZ)   for SIG=+1
% 
%   SIG is either +1 or -1 and refers to the superscript of the spherical
%   harmonic function. For M=0, SPHARMONIC only calculates the term for
%   SIG=+1, since the solution for SIG=-1 is always trivial (i.e. zero).
%   P(N,M;sin(ELEV)) is the Legendre polynomial of degree N and order M,
%   calculated at sin(ELEV) by the means of the Matlab function LEGENDRE.
%
%   Examples: 
%     1. spharmonic(2 , 0.0:0.1:0.2, 0.3:0.1:0.5) returns the matrix:
% 
%                   | az=0, el=0.3        az=0.1, el=0.4      az=0.2,el=0.5
%        -----------|------------------------------------------------------
%        m*sig = -2 |         0             0.5056              0.8997
%        m*sig = -1 |         0            -0.1074             -0.2508
%        m*sig =  0 |   -0.3690            -0.2725             -0.1552
%        m*sig =  1 |   -0.8470            -1.0707             -1.2370
%        m*sig =  2 |    2.7380             2.4943              2.1281
%
%     2. AZ = rand(2,4,5); ELEV = rand(2,4,5); N = 2; 
%        Y = spharmonic(N,AZ,ELEV); 
%
%     so that size(Y) is 5x2x4x5 and 
%     Y(:,1,2,3) is the same as spharmonic(N,AZ(1,2,3),ELEV(1,2,3)). 
%
%   See also: LEGENDRE.


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ----- INPUT ARGUMENT CHECKING AND PROCESSING, CONSTANT DEFINITIONS -----

% Check number of input arguments
if nargin < 3
    error('Not enough input arguments')
elseif nargin > 4
    error('Too many input arguments')    
end


% Check 'degree' input argument
if ~isnumeric(degree) || ~isscalar(degree) || floor(degree) ~= degree || degree < 0
    error('Unknown ''degree'' argument. Must be non-negative integer.');
end


% Check for matching sizes of azimuth and elevation arrays
if (size(azimuth)) ~= (size(elevation))
   error('Input arguments for azimuth and elevation must have the same size');
end

% Save original dimensions for later restoration
sizeAz = size(azimuth);


% Check 'normalize' input argument
if ~exist('normalize','var') || isempty(normalize)
    % Default to 'unnorm' if not specified
    normalize = 'unnorm';
end


% ----------------------------- CALCULATIONS -----------------------------

% Convert to single row vectors for internal calculations
azimuth = azimuth(:)';
elevation = elevation(:)';

% Calculate legendre functions for lower rows with m*sig = 0:degree
legFctPlus = legendre(degree, sin(elevation), normalize);

% Calculate legendre functions for upper rows with m*sig = -degree:0
legFctMinus = flipud(legFctPlus);

% Remove row for m=0 and sig=-1, since it always returns zero(s)
legFctMinus(end,:) = [];

% Calculate upper and lower half of the spherical harmonics
Yminus = legFctMinus .* sin(kron((degree:-1:1)',azimuth));
Yplus  = legFctPlus .* cos(kron((0:1:degree)',azimuth));

% Marry the upper and lower half of the spherical harmonics
Y = [Yminus; Yplus];

% Restore original dimensions
Y = reshape(Y,[2*degree+1 sizeAz]);

% Remove singleton dimensions
Y = squeeze(Y);

return