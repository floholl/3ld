function speakerGain = vbp(sourceDirection,speakerDirection,speakerGroups,type,sourceGain,areSourcesIdentical)

%3LD :: VBP Vector Base Panning loudspeaker gains.
%   G = VBP(SRC,SPK,GROUP[,TYPE,GAIN,IDENTICAL]) calculates the gains
%   of an array of L loudspeakers due to S vector-base panned virtual sound
%   sources. 2D or 3D VBP can be applied.
%
%   SRC specifies the directions of the virtual sound sources. For 2D VBP,
%   it is an S-by-1 array representing the azimuths, whereas for 3D VBAP
%   VBAP, it is an S-by-2 array representing azimuths and elevations. All
%   angles have to be specified in radians. The same left-oriented
%   coordinate system as in SPH2CART is applied.
%
%   SPK specifies the directions of the loudspeakers. It can be an L-by-1
%   (2D VBP) or an L-by-2 (3D VBP) array, and is interpreted in the same
%   way as SRC.
%
%   GROUP specifies pairs (2D VBP) or triples (3D VBP) of loudspeakers.
%   and thus is an R-by-2 or R-by-3 matrix, where R is the number of pairs/
%   triples. The entries of GROUP represent the row indices of the
%   respective loudspeakers in SPK.
%
%   TYPE is a string that specifies whether vector base amplitude panning
%   [1] or vector base intensity panning [2] is applied. For the first
%   case, the string should be 'vbap' or 'a'. For the second case, 'vbip'
%   or 'i' are legal. If not specified, this argument defaults to 'vbap'.
%
%   GAIN specifies the gains of the virtual sound sources. It can be a
%   scalar, in which case it is applied to all sources, or an S-by-1 array,
%   if different gain factors for each source are to be applied. Additional
%   values will be ignored, and missing values will be set to 1. If not
%   specified, GAIN defaults to 1.
%
%   IDENTICAL specifies whether the spatialized sound sources are fed by the
%   same audio signal. If IDENTICAL is not zero, this is assumed to be the
%   case, while independent sources are assumed if IDENTICAL is 0. This
%   affects the dimensions of the output array G. If not specified, IDENTICAL
%   defaults to 0.
%
%   The output array G represents the gains of the loudspeakers. It is
%   an L-by-1 matrix if the sound sources are fed by the same audio signal
%   (i.e. IDENTICAL=1) or an L-by-S matrix if not (i.e. IDENTICAL=0). In
%   the latter case, the columns of G represent the gain factors for the
%   S independent sound sources. Note that the function does by no means
%   normalize the output gain factors. It is your responsibility to avoid
%   clipping.
%
%   Example:
%   Reproduce a front source on an octahedron loudspeaker layout
%       source_position = [0 0];
%       p = platonicsolid('oct');
%       [az elev] = cart3sph(p.vertices);
%       g = vbp(source_position,[az elev],p.faces,'vbip')
%
%   See also: AMB3D_ENCODER (3LD), AMB3D_DECODER (3LD),
%             AMB3D_REGULARITY (3LD).
%
%   Reference:
%     [1] Ville Pulkki: Virtual Sound Source Positioning Using Vector Base
%         Amplitude Panning. Journal of the Audio Engineering Society,
%         Vol.45, No.6, June 1997, pp. 456-466
%     [2] Jean-Marie Pernaux, Patrick Boussard, Jean-Marc Jot: Virtual
%         Sound Source Positioning and Mixing in 5.1. Implementation on the
%         Real-Time System Genesis. Proceedings DAFX98, Barcelona


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% --------------------------------- NOTES ---------------------------------
%
% Possible improvements for future version:
%   The accuracy of the gain factor scaledowns for VBAP and VBIP has to be
%   evaluated for the case of multiple sound sources (line 229)


% ----- INPUT ARGUMENT CHECKING AND PROCESSING, CONSTANT DEFINITIONS -----

% Check number of input arguments
if nargin < 3
    error('Not enough input arguments.')
elseif nargin > 6
    error('Too many input arguments.')    
end


% Check 'sourceDirection' input argument
if size(sourceDirection,2) ~= 1  &&  size(sourceDirection,2) ~= 2
    error('Illegal dimensions of ''sourceDirection'' input array. Must be numSources-by-2 or numSources-by-1 array.');
end
% Convert to cartesian coordinates
if size(sourceDirection,2) == 1
    [sourceDirection(:,1), sourceDirection(:,2)] = pol2cart(sourceDirection(:,1),1);
elseif  size(sourceDirection,2) == 2
    [sourceDirection(:,1), sourceDirection(:,2), sourceDirection(:,3)] = sph2cart(sourceDirection(:,1),sourceDirection(:,2),1);
end
% Number of virtual sound sources
numSources = size(sourceDirection,1);


% Check 'sourceDirection' input argument
if size(speakerDirection,2) ~= 1  &&  size(speakerDirection,2) ~= 2
    error('Illegal dimensions of ''speakerDirection'' input array. Must be numSpeakers-by-2 or numSpeakers-by-1 array.');
end
% Convert to cartesian coordinates
if size(speakerDirection,2) == 1
    [speakerDirection(:,1), speakerDirection(:,2)] = pol2cart(speakerDirection(:,1),1);
elseif  size(speakerDirection,2) == 2
    [speakerDirection(:,1), speakerDirection(:,2), speakerDirection(:,3)] = sph2cart(speakerDirection(:,1),speakerDirection(:,2),1);
end
numSpeakers = size(speakerDirection,1);


% Check 'speakerGroups' input argument
if size(speakerGroups,2) ~= 2  &&  size(speakerGroups,2) ~= 3
    error('Illegal dimensions of ''speakerGroups'' input array. Must be numGroups-by-3 or numGroups-by-2 array.');
end
% Number of loudspeaker pairs/triples
numGroups = size(speakerGroups,1);

% Check if all input arrays assume either 2D or 3D VBP
if ~isequal(size(sourceDirection,2),size(speakerDirection,2),size(speakerGroups,2))
    error('''sourceDirection'' and ''speakerDirection'' must have the same number of columns, and ''speakerGroups'' must have one more than these.');
end


% Check 'type' input argument
if  ~exist('type','var') || isempty(type)
    % Default to 'vbap' if not specified
    type = 'vbap';
else
    % Safety
    type = lower(type);
    if strcmp(type,'vbap') || strcmp(type,'a')
        type = 'vbap';
    elseif strcmp(type,'vbip') || strcmp(type,'i')
        type = 'vbip';
    else
        error('Unknown ''type'' input argument. Must be ''vbap''/''a'' or ''vbip''/''i''.');
    end
end


% Check 'sourceGain' input argument
if  ~exist('sourceGain','var') || isempty(sourceGain)
    % Default to 1 if not specified
    sourceGain = 1;
end
if isscalar(sourceGain)
    % Apply to all sources if specified as a scalar
    sourceGain = repmat(sourceGain,1,numSources);
else
    % Ignore additional values
    if numel(sourceGain) > size(sourceDirection,1)
        sourceGain = sourceGain(1:size(sourceDirection,1));
        warning('Clipped oversized ''sourceGain'' input array to required amount of entries.')
    % Set missing values to one
    elseif numel(sourceGain) < size(sourceDirection,1)
        sourceGain(end+1:size(sourceDirection,1)) = 1;
        warning('Set missing entries in ''sourceGain'' input array to one.');
    end
    % Make this a row vector for internal calculations
    sourceGain = sourceGain(:)';
end


% Check 'areSourcesIdentical' input argument
if ~exist('areSourcesIdentical','var') || isempty(areSourcesIdentical)
    % Default to 0 if not specified
    areSourcesIdentical = 0;
end



% ----------------------------- CALCULATIONS -----------------------------

% Initialize, so our output array gets right dimensions
speakerGain = zeros(numSpeakers,numSources);


% For every virtual sound source...
% TODO: NESTED FOR LOOP COULD BE REPLACED BY MATRIX OPERATIONS IN FUTURE VERSIONS
for i = 1 : numSources
    
    % Initialization for later use of 'end'
    localGain = [];
    
    % Build up a numGroups-by-2/numGroups-by-3 matrix with the gains of all
    % loudspeaker groups for this sound source
    for j = 1 : numGroups
        
        % Get loudspeaker positions for this group
        thisGroup = speakerDirection(speakerGroups(j,:),:);
        
        % Get speakerGain factors for this group (eq.18 in [1])
        localGain(end+1,:) = sourceDirection(i,:) * inv(thisGroup);
        
    end

    % The right triple is the one with only positive gains, but in [1], it is
    % recommended to choose the triple with the 'highest smallest factor' to
    % avoid wrong choices due to numerical inaccuracies.
    [val,grpIdx] = max( min(localGain,[],2) );
    localGain = localGain(grpIdx,:);

    % Get the speaker indices of the chosen loudspeaker triple
    spkIdx = speakerGroups(grpIdx,:);

    % Assign the gains of these speakers to the right rows in the output array
    speakerGain(spkIdx,i) = localGain;

end


% Scale down loudspeaker gains
% TODO: THIS IS THE WEAK POINT OF THE VBP FUNCTION: THE ACCURACY OF THE
% SCALEDOWNS IN THE CASE OF MULTIPLE SOUND SOURCES NEES TO BE CHECKED FOR
% BOTH, VBAP AND VBIP!
if strcmp(type,'vbap')
    % Scale according to Vector Base Amplitude Panning (eq.19 in [1])
    scaleFactor = sourceGain ./ sqrt( sum(speakerGain.^2) );
    scaleFactor = repmat(scaleFactor,numSpeakers,1);
    speakerGain = speakerGain .* scaleFactor;
    
elseif strcmp(type,'vbip')
    % Scale according to Vector Base Intensity Panning (see [2])
    scaleFactor = sourceGain ./ sum(speakerGain);
    scaleFactor = repmat(scaleFactor,numSpeakers,1);
    speakerGain = sqrt(scaleFactor .* speakerGain);

end


% Superpone the loudspeaker gains in the case of identical source signals
if areSourcesIdentical ~= 0
    speakerGain = sum(speakerGain,2);
end


return