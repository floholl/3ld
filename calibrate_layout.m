function [calibGain, calibDelay] = calibrate_layout(speakerPosition,speedOfSound)

%3LD :: CALIBRATE_LAYOUT Calibrates a periphonic loudspeaker layout.
%   [G,D] = CALIBRATE_LAYOUT(SPK[,C]) calculates the calibration gains G and
%   delays D for an array of loudspeaker with varying radii. SPK is a
%   L-by-3 array representing the x,y,z coordinates of the L loudspeakers
%   in the array. The same cartesian coordinate system as in Matlab's
%   SPH2CART is applied. C is the speed of sound, which defaults to 343.3
%   meters per second (air at 20�C and at sea level) if not specified.
%
%   G is a vector of length L, representing the gain factors of all
%   loudspeakers in the array due to the 1/r law of sound pressure
%   amplitude decay. The speaker with the greatest radius will be assigned
%   a gain factor of 1, whereas the gains of the closer speakers will be
%   attenuated to factors < 1.
%
%   D is a vector of length L, representing the delay factors of all
%   loudspeakers in the array due to the finite speed of sound C in
%   seconds. The speaker with the greatest radius will be assigned a gain
%   factor of 0.
%
%   Note that loudspeaker array calibration results in the virtual sound
%   sources moving at a distance which is equal to the maximum radius of a
%   loudspeaker in the array, i.e. the distance of the loudspeaker which
%   the layout is normalized too.
%
%   Example:
%   Create a platonic solid using a 3LD function and move one of its
%   vertices to a greater radius. Calculate the calibration gains and
%   delays for loudspeakers positioned at the vertices of the solid.
%       p = platonicsolid('tetra');
%       p.vertices(1,:) = [0.8, 0.8, 0.8];
%       p.vertices
%       [g,d] = calibrate_layout(p.vertices)
%
%   See also: MAP_TO_SURFACE (3LD).


% --------------------------- COPYRIGHT NOTICE ---------------------------

% 3LD - Library for Loudspeaker Layout Design; release 2, 2006/03/15
% Copyright (c) 2006 Florian Hollerweger, floholl_AT_sbox.tugraz.at

% This file is part of 3LD. 3LD is free software; you can redistribute it
% and/or modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of the
% License, or (at your option) any later version. 3LD is distributed in the
% hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
% See the GNU General Public License for more details. You should have
% received a copy of the GNU General Public License along with 3LD; if not,
% write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
% Boston, MA  02110-1301 USA


% ------------------------ INPUT ARGUMENT CHECKING ------------------------

% Check number of input arguments
if nargin < 1
    error('Not enough input arguments.')
elseif nargin > 2
    error('Too many input arguments.')    
end

% Check 'speakerPosition' input argument
if ~isequal(ndims(speakerPosition),2) || ~isequal(size(speakerPosition,2),3)
    error('Invalid ''speakerPosition'' input argument. Must be L-by-3 array.');
end

% Check 'speedOfSound' input argument
if ~exist('speedOfSound','var') || isempty(speedOfSound)
    % Speed of sound for air at 20�C and at sea level in meters per second
    speedOfSound = 343.3;
elseif ~isnumeric(speedOfSound) || ~isscalar(speedOfSound)
    error('Invalid ''speedOfSound'' input argument. Must be numeric scalar.');
end


% ----------------------------- CALCULATIONS -----------------------------

% Get the loudspeaker radii
[az,elev,r] = cart2sph(speakerPosition(:,1), speakerPosition(:,2), speakerPosition(:,3));

% Calculate the calibration gain factors of all loudspeakers
calibGain = r / max(r);
    
% Calibration time [sec], where calibDelay=0 for speaker with maximal radius
calibDelay = (max(r) - r) / speedOfSound;